package com;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.main.userProfile.UserProfileInit;

@Component
public class StartUpInit {
	
	@Autowired
	public UserProfileInit userProfileInit;
	@PostConstruct
	public void init(){
		userProfileInit.getInstance();
	}
}
