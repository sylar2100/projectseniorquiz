package com.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.budget.services.SelectServiceImpl;
import com.entities.BrowseObjectResponse;
import com.entities.Customer;
import com.entities.ResponseObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@RestController
public class RestWebController {
	
	List<Customer> cust = new ArrayList<Customer>();
	@Autowired
    private SelectServiceImpl selectServiceImpl;
	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private BrowseObjectResponse browseObjectResponse;
	JSONParser parser = new JSONParser();
	Gson gson = new Gson();
	
	@RequestMapping(value = "/getallcustomer", method = RequestMethod.GET)
	public List<Customer> getResource(){
			return cust;
	}
	
	@RequestMapping(value="/postcustomer", method=RequestMethod.POST)
	public String postCustomer(@RequestBody Customer customer) throws ParseException, IOException {
		responseObject = selectServiceImpl.selectService(customer);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		return gson.toJson(responseObject);
	}
	
	@RequestMapping(value="*/postcustomer", method=RequestMethod.POST)
	public String postCustomerAll(@RequestBody Customer customer) throws ParseException, IOException {
		responseObject = selectServiceImpl.selectService(customer);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		return gson.toJson(responseObject);
	}
	
	@RequestMapping(value="*/browsedata", method=RequestMethod.POST)
	public String postBrowse(@RequestBody Customer customer) throws ParseException, IOException{
		browseObjectResponse = selectServiceImpl.selectBrowse(customer);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		return gson.toJson(browseObjectResponse);
	}
}