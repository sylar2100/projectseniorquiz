'use strict';

// var app = angular.module( 'puridiomSeedApp', [] );

var appLogin = angular.module( 'quizApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'datatables'
  ] )
  .config( function ( $routeProvider,$locationProvider ) {
    $routeProvider
      .when('/', {
        controller: 'LoginController',
        templateUrl: 'views/login.html'
      })
      .when( '/register', {
        controller: 'LoginController',
        templateUrl: 'views/register.html'
      } )
      .when( '/user', {
        controller: 'LoginController',
        templateUrl: 'views/user.html'
      } )
      .when( '/browse', {
        controller: 'BrowseController',
        templateUrl: 'views/browse.html'
      } )
      .when( '/browseEmpty', {
        controller: 'BrowsePopulateController',
        templateUrl: 'views/emptybrowse.html'
      } )
      .otherwise( {
        redirectTo: '/'
      } );
    $locationProvider.html5Mode({
    	  enabled: true,
    	  requireBase: false
    });
  } );

appLogin.factory("MyService", function() {
	// This service I create this for pass parameters from controller to controller
	return {
		data: {}
	};
});

