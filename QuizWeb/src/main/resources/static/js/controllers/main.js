'use strict';

angular.module('quizApp').controller('MainCtrl', function($scope, $http, MyService, dataService) {

	// Create request, requestHeader and requestBody
	$scope.request = {};
	$scope.request.requestHeader = {};
	$scope.request.requestBody = {};
	MyService.data.url = '';
	MyService.data.params = '';
	
	dataService.getData(function(response) {
		// Update the requisitionHeader with the data of the response
		$scope.requisitionHeader = response.responseResult.result.header;
		// Create and update the requisitionObject with the data of the response
		$scope.requisitionObject = {};
		$scope.requisitionObject.header = $scope.requisitionHeader;

		// Update the request with the data of the response and requisitionObject
		$scope.request.requestHeader = response.requestHeader;
		$scope.request.requestBody = $scope.requisitionObject;
	});

	$scope.reload = function() {
		MyService.data.url = '/puridiom/jsonServlet';
		MyService.data.params = 'request=' + JSON.stringify($scope.request);
		
		dataService.getData(function(response) {
			// Update the requisitionHeader with the data of the response
			$scope.requisitionHeader = response.responseResult.result.header;
			// Create and update the requisitionObject with the data of the response
			$scope.requisitionObject = {};
			$scope.requisitionObject.header = $scope.requisitionHeader;

			// Update the request with the data of the response and requisitionObject
			$scope.request.requestHeader = response.requestHeader;
			$scope.request.requestBody = $scope.requisitionObject;
		});
	};
});