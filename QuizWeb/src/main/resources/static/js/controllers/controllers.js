appLogin.controller('LoginController', function($scope, $location, $http, MyService, dataService) {    
	//This scope is a for combo box and display the languages
	
	$scope.register = function() {
		$location.path('register');
	};
	$scope.transferCoinsCalculate = function() {
		var obj = {};
		obj.userSend = $scope.user.userId;
		obj.transferCoin = $scope.transferCoins;
		obj.userReceive = $scope.transferCoinsUser;
    	$scope.setMyService('/postcustomer','transfer', 'transfer',obj);
	    //With this function I call my http for get the data
		dataService.getData(function(response) {
			if( response.statusCode !== "200") {
				$scope.errorsMessage = response.message;
				return;
			} else {
				MyService.urlParams = {};
				MyService.urlParams.obj = 'user';
				MyService.urlParams.operation = 'allUsers';
				MyService.urlParams.namePage = 'browse';
				$location.path('browseEmpty');
			}
	    });
    };
	
    $scope.setMyService = function(postMethod, objName, operation, obj) {
		MyService.data.url = $location.absUrl() + postMethod;
		MyService.data.params = {
			handler: objName,
			operation: operation
	    };
		if(objName == 'transfer') {
			MyService.data.params.transferObject = obj;
		} else {
			MyService.data.params.user = obj;
		}
    };
    
    if(MyService.urlParams != undefined) {
		$scope.user = MyService.urlParams.entity;
		$scope.setMyService('/postcustomer','dataInit', 'retrieveDataInit','');
		dataService.getData(function(response) {
			if(response) {
				$scope.userProfileList = response.userProfileList;
				$scope.mapData = response.mapData;
			}
		});
	}
    
	$scope.submitForm = function() {
		//only send params when exist requestHeader this is for the second attempt
		//if send the requestHeader in the controller give undefined for this reason only send requestHeader when exist value
		MyService.data.url = $location.absUrl() + "postcustomer";
	    MyService.data.params = {
    		handler: 'userProfile',
    		operation: 'authentication',
    		user: {
    			userId: $scope.userId,
    			userPassword: $scope.password
    		}
	    };

	    //With this function I call my http for get the data
		dataService.getData(function(response) {
			if( response.statusCode !== "200") {
				$scope.errorsMessage = response.message;
				return;
			} else {
				MyService.urlParams = {};
				MyService.urlParams.obj = 'user';
				MyService.urlParams.operation = 'allUsers';
				MyService.urlParams.namePage = 'browse';
				$location.path('browseEmpty');
			}
	    });
	};
});

appLogin.controller('BrowseController', function($scope, $window, $location, $http, MyService, dataService, DTOptionsBuilder, DTColumnBuilder) {    
	//This scope is a for combo box and display the languages
	if(MyService.data.browseList) {
    	$scope.browseList = MyService.data.browseList.objResponse;
		$scope.columnList = MyService.data.browseList.columns;
		$scope.labelList = MyService.data.browseList.labels;
		$scope.needButtons = MyService.data.browseList.needButtons;
		$scope.viewButton = MyService.data.browseList.viewButton;
		$scope.imagePath = MyService.data.browseList.imagePath;
	}
	$scope.openAccount = function(entity) {
		MyService.urlParams.operation = 'update';
		MyService.urlParams.entity = entity;
		$location.path('user');
	};
	$scope.delete = function(entity) {
		MyService.urlParams.operation = 'delete';
		MyService.urlParams.entity = entity;
		$location.path(MyService.urlParams.obj);
	};
	
	MyService.data.browseList = [];
	MyService.data.columnList = [];
	MyService.data.labelList = [];
	MyService.data.needButtons = false;
	MyService.data.viewButton = false;
	
    $scope.vm = {};

	$scope.vm.dtOptions = DTOptionsBuilder.newOptions()
	  .withOption('order', [0, 'asc']);
	  
});

appLogin.controller('BrowsePopulateController', function($scope, $window, $location, $http, MyService, dataService, DTOptionsBuilder, DTColumnBuilder) {    
	//This scope is a for combo box and display the languages
	MyService.data.url = $location.absUrl() + "/browsedata";
	MyService.data.params = {
		handler: MyService.urlParams.obj,
		operation: MyService.urlParams.operation,
		browseName: MyService.urlParams.browseName
    };
    
    dataService.getData(function(response) {
    	MyService.data.browseList = response;    	
		$location.path(MyService.urlParams.namePage);
    });
	  
});
