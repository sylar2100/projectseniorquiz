var app = angular.module("puridiomSeedApp", []);
app.directive('ngGraphchart',function() {
	var datos = [{"label":"REQ IN PROGRESS", "value":123,"code":"1000"}, 
		         {"label":"REQ APPROVING", "value":26,"code":"1030"},
		         {"label":"REQ APPROVED", "value":50,"code":"1035"},
		         {"label":"PO IN PROGRESS", "value":1,"code":"3000"},
		         {"label":"PO AWARDED", "value":31,"code":"3030"},
		         {"label":"RCV RECEIVED", "value":1,"code":"4010"}];
	return {
		/* Restrict option is used to specify how a directive can be invoked on the page
		 * 'A' - <span ng-graphchart></span>
		 * 'E' - <ng-graphchart></ng-graphchart> 
		 * 'C' - <span class="ng-graphchart"></span> 
		 * */
		restrict : 'A',
		//ng-graphtype required as dependency
		require : '^ngGraphtype',
		// transclude
		transclude : true,
		// This will create an empty scope
		scope : {
			//Store the string associated to ng-graphtype from parent scope
			ngGraphtype : '@'
		},
		// This template is an inline template where we are specifying the html that will be appended  
		template : '<div class="Graph"><div ng-transclude></div><div class="graph"></div></div>',
		controller: ['$scope', '$http', function($scope, $http) {
		}],
		/*
		scope
		iElement
		iAttrs
		ctrl 
		*/
		link : function(scope, iElement, iAttrs, ctrl) {
			console.log('Datos : '+ datos);
			console.log('NgGraphType : '+ iAttrs.ngGraphtype);
			data = datos;
			gType = iAttrs.ngGraphtype;
			//Choose what type of graph is and built that graph based on ng-graphtype value
			if(gType === 'pie'){
				//Graph pieGraph = new PieGraph();
				chartPieGraph(iElement, data, iAttrs);
			} else if(gType === 'bar'){
				chartBarGraph(iElement, data, iAttrs);
			} else if(gType === 'gauge'){
				//Setting features
				var config = {size: 300,clipWidth: 300,clipHeight: 300,ringWidth: 60,maxValue: 10,transitionMs: 4000,};
				//
				var gaugeGraph = chartGaugeGraph(iElement, data, config);
				//
				gaugeGraph.render();
				//
				gaugeGraph.update(Math.random() * 10);
			}
		}
	};
});

app.directive('ngGraphtype', function() {
	return {
		controller : function($scope) {
		}
	};
});


var chartPieGraph = function(element, data, opts) {
	console.log(element);
	var w = 300, //width
	h = 300, //height
	r = 100, //radius
	color = d3.scale.category20c(); //builtin range of colors

	var vis = d3.select(element[0]).append("svg:svg") //create the SVG element inside the target element
	.data([ data ]) //associate our data with the element
	.attr("width", w) //set the width and height of our visualization (these will be attributes of the <svg> tag
	.attr("height", h).append("svg:g") //make a group to hold our pie chart
	.attr("transform", "translate(" + r + "," + r + ")"); //move the center of the pie chart from 0, 0 to radius, radius

	var arc = d3.svg.arc() //this will create <path> elements for us using arc data
	.outerRadius(r);

	var pie = d3.layout.pie() //this will create arc data for us given a list of values
	.value(function(d) {
		return d.value;
	}); //we must tell it out to access the value of each element in our data array

	var arcs = vis.selectAll("g.slice") //this selects all <g> elements with class slice (there aren't any yet)
	.data(pie) //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties) 
	.enter() //this will create <g> elements for every "extra" data element that should be associated with a selection. The result is creating a <g> for every object in the data array
	.append("svg:g") //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
	.attr("class", "slice"); //allow us to style things in the slices (like text)

	arcs.append("svg:path").attr("fill", function(d, i) {
		return color(i);
	}) //set the color for each slice to be chosen from the color function defined above
	.attr("d", arc); //this creates the actual SVG path using the associated data (pie) with the arc drawing function
	arcs.append("svg:text") //add a label to each slice
	.attr("transform", function(d) { //set the label's origin to the center of the arc
		//we have to make sure to set these before calling arc.centroid
		d.innerRadius = 0;
		d.outerRadius = r;
		return "translate(" + arc.centroid(d) + ")"; //this gives us a pair of coordinates like [50, 50]
	}).attr("text-anchor", "middle") //center the text on it's origin
	.text(function(d, i) {
		return data[i].label;
	}); //get the label from our original data array

};


var chartBarGraph = function (element,data,opts) {

	var valueLabelWidth = 40; // space reserved for value labels (right)
	var barHeight = 20; // height of one bar
	var barLabelWidth = 100; // space reserved for bar labels
	var barLabelPadding = 5; // padding between bar and bar labels (left)
	var gridLabelHeight = 18; // space reserved for gridline labels
	var gridChartOffset = 3; // space between start of grid and first bar
	var maxBarWidth = 420; // width of the bar with the max value
	 
	// accessor functions 
	//var barLabel = function(d) { return d['Name']; };
	//var barValue = function(d) { return parseFloat(d['Population (mill)']); };
	var barLabel = function(d) { return d['label']; };
	var barValue = function(d) { return d['value']; };
	// scales
	var yScale = d3.scale.ordinal().domain(d3.range(0, data.length)).rangeBands([0, data.length * barHeight]);
	var y = function(d, i) { return yScale(i); };
	var yText = function(d, i) { return y(d, i) + yScale.rangeBand() / 2; };
	var x = d3.scale.linear().domain([0, d3.max(data, barValue)]).range([0, maxBarWidth]);
	// svg container element
	var chart = d3.select(element[0]).append("svg").attr('width', maxBarWidth + barLabelWidth + valueLabelWidth).attr('height', gridLabelHeight + gridChartOffset + data.length * barHeight);
	// grid line labels
	var gridContainer = chart.append('g').attr('transform', 'translate(' + barLabelWidth + ',' + gridLabelHeight + ')'); 
	gridContainer.selectAll("text").data(x.ticks(10)).enter().append("text").attr("x", x).attr("dy", -3).attr("text-anchor", "middle").text(String);
	// vertical grid lines
	gridContainer.selectAll("line").data(x.ticks(10)).enter().append("line").attr("x1", x).attr("x2", x).attr("y1", 0).attr("y2", yScale.rangeExtent()[1] + gridChartOffset).style("stroke", "#ccc");
	// bar labels
	var labelsContainer = chart.append('g').attr('transform', 'translate(' + (barLabelWidth - barLabelPadding) + ',' + (gridLabelHeight + gridChartOffset) + ')'); 
		labelsContainer.selectAll('text').data(data).enter().append('text').attr('y', yText).attr('stroke', 'none').attr('fill', 'black').attr("dy", ".35em") .attr('text-anchor', 'end').text(barLabel);
	// bars
	var barsContainer = chart.append('g').attr('transform', 'translate(' + barLabelWidth + ',' + (gridLabelHeight + gridChartOffset) + ')'); 
	barsContainer.selectAll("rect").data(data).enter().append("rect").attr('y', y).attr('height', yScale.rangeBand()).attr('width', function(d) { return x(barValue(d)); }).attr('stroke', 'white').attr('fill', 'steelblue');
	// bar value labels
	barsContainer.selectAll("text").data(data).enter().append("text").attr("x", function(d) { return x(barValue(d)); }).attr("y", yText).attr("dx", 3).attr("dy", ".35em").attr("text-anchor", "start").attr("fill", "black").attr("stroke", "none").text(function(d) { return d3.round(barValue(d), 2); });
	// start line
	barsContainer.append("line").attr("y1", -gridChartOffset).attr("y2", yScale.rangeExtent()[1] + gridChartOffset).style("stroke", "#000");
};

var chartGaugeGraph = function(container,data, configuration) {
	var that = {};
	var config = {
		size: 200, clipWidth: 200, clipHeight: 110,
		ringInset: 20, ringWidth: 20, pointerWidth: 10,
		pointerTailLength: 5, pointerHeadLengthPercent: 0.9,
		minValue: 0, maxValue: 10,
		minAngle: -90, maxAngle: 90,
		transitionMs: 750, majorTicks: 5,
		labelFormat: d3.format(',g'),
		labelInset: 10,
		arcColorFn: d3.interpolateHsl(d3.rgb('#e8e2ca'), d3.rgb('#3e6c0a'))
	};
	var range = undefined;
	var r = undefined;
	var pointerHeadLength = undefined;
	var value = 0;
	
	var svg = undefined;
	var arc = undefined;
	var scale = undefined;
	var ticks = undefined;
	var tickData = undefined;
	var pointer = undefined;

	var donut = d3.layout.pie();
	
	function deg2rad(deg) {
		return deg * Math.PI / 180;
	}
	
	function newAngle(d) {
		var ratio = scale(d);
		var newAngle = config.minAngle + (ratio * range);
		return newAngle;
	}
	
	function configure(configuration) {
		var prop = undefined;
		for ( prop in configuration ) {
			config[prop] = configuration[prop];
		}
		
		range = config.maxAngle - config.minAngle;
		r = config.size / 2;
		pointerHeadLength = Math.round(r * config.pointerHeadLengthPercent);

		// a linear scale that maps domain values to a percent from 0..1
		scale = d3.scale.linear().range([0,1]).domain([config.minValue, config.maxValue]);
			
		ticks = scale.ticks(config.majorTicks);
		tickData = d3.range(config.majorTicks).map(function() {return 1/config.majorTicks;});
		//draw Arc
		arc = d3.svg.arc().innerRadius(r - config.ringWidth - config.ringInset).outerRadius(r - config.ringInset).startAngle(function(d, i) {
			var ratio = d * i;
			return deg2rad(config.minAngle + (ratio * range));
		}).endAngle(function(d, i) {
			var ratio = d * (i+1);
			return deg2rad(config.minAngle + (ratio * range));
		});
	}
	that.configure = configure;
	
	function centerTranslation() {
		return 'translate('+r +','+ r +')';
	}
	
	function isRendered() {
		return (svg !== undefined);
	}
	that.isRendered = isRendered;
	
	function render(newValue) {
		//console.log('Gauge Render');
		svg = d3.select(container[0]).append('svg:svg').attr('class', 'gauge').attr('width', config.clipWidth).attr('height', config.clipHeight);
		var centerTx = centerTranslation();
		var arcs = svg.append('g').attr('class', 'arc').attr('transform', centerTx);
		arcs.selectAll('path').data(tickData).enter().append('path').attr('fill', function(d, i) {
			return config.arcColorFn(d * i);
		}).attr('d', arc);
		var lg = svg.append('g').attr('class', 'label').attr('transform', centerTx);
		lg.selectAll('text').data(ticks).enter().append('text').attr('transform', function(d) {
			var ratio = scale(d);
			var newAngle = config.minAngle + (ratio * range);
			return 'rotate(' +newAngle +') translate(0,' +(config.labelInset - r) +')';
		}).text(config.labelFormat);
		var lineData = [ [config.pointerWidth / 2, 0], [0, -pointerHeadLength],[-(config.pointerWidth / 2), 0],[0, config.pointerTailLength],[config.pointerWidth / 2, 0] ];
		var pointerLine = d3.svg.line().interpolate('monotone');
		var pg = svg.append('g').data([lineData]).attr('class', 'pointer').attr('transform', centerTx);
		pointer = pg.append('path').attr('d', pointerLine).attr('transform', 'rotate(' +config.minAngle +')');
		update(newValue === undefined ? 0 : newValue);
	}
	that.render = render;
	
	function update(newValue, newConfiguration) {
		if ( newConfiguration  !== undefined) {
			configure(newConfiguration);
		}
		var ratio = scale(newValue);
		var newAngle = config.minAngle + (ratio * range);
		pointer.transition()
			.duration(config.transitionMs)
			.ease('elastic')
			.attr('transform', 'rotate(' +newAngle +')');
	}
	that.update = update;

	configure(configuration);
	
	return that;
};