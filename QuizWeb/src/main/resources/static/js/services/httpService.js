//Service for implements all method for the HTTP
appLogin.service("dataService", function($http, MyService) {
	this.getData = function(callbackFunc) {
		$http({
			method: 'POST',
			url: MyService.data.url,
			data: MyService.data.params,
			headers: {'Accept': 'text/plain'}
		}).success(function(data){
			callbackFunc(data);
	    }).error(function(){
	        //alert("error");
	    });
	};
});

