package com.budget.services;

import com.entities.Movies;
import com.entities.ResponseObject;

public interface MovieService {
	   public abstract ResponseObject selectMovieOperation(String opt, Movies movie);
}
