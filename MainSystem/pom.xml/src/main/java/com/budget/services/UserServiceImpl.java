package com.budget.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.ResponseObject;
import com.entities.UserProfile;
import com.main.userProfile.AddUserProfile;
import com.main.userProfile.DeleteUserProfile;
import com.main.userProfile.UpdateUserProfile;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddUserProfile addUser;
	@Autowired
	private UpdateUserProfile updateUser;
	@Autowired
	private DeleteUserProfile deleteUser;
	
	public ResponseObject selectUserOperation(String opt, UserProfile user) {
		if(opt.equalsIgnoreCase("add")) {
			addUser.executeClass(user);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("update")) {
			updateUser.executeClass(user);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteUser.executeClass(user);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		return responseObject;
	}
}
