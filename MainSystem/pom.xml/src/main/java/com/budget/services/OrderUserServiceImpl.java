package com.budget.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountLineRepositoryImpl;
import com.budget.repository.AccountRepositoryImpl;
import com.budget.repository.CatalogItemRepositoryImpl;
import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.entities.CatalogoItem;
import com.entities.Customer;
import com.entities.OrderUser;
import com.entities.ResponseObject;
import com.main.account.AddAccount;
import com.main.account.DeleteAccount;
import com.main.account.UpdateAccount;
import com.main.orderuser.AddOrderUser;
import com.main.orderuser.DeleteOrderUser;
import com.main.orderuser.UpdateOrderUser;

@Service
public class OrderUserServiceImpl implements OrderUserService {

	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddOrderUser addOrderUser;
	@Autowired
	private UpdateOrderUser updateOrderUser;
	@Autowired
	private DeleteOrderUser deleteOrderUser;
	@Autowired
	private CatalogItemRepositoryImpl catalogItemRepositoryImpl;
	@Autowired
	private AccountRepositoryImpl accountRepositoryImpl;
	@Autowired
	private Customer customer;

	public ResponseObject selectOrderUserOperation(String opt, OrderUser order) {
		if(opt.equalsIgnoreCase("add")) {
			addOrderUser.executeClass(order);
			responseObject.setMessage("Add Order Succesfully");
			responseObject.setStatusCode("200");
			responseObject.setObjResponse(order);
		}
		if(opt.equalsIgnoreCase("update")) {
			updateOrderUser.executeClass(order);
			responseObject.setMessage("Update Order Succesfully");
			responseObject.setStatusCode("200");
			responseObject.setObjResponse(order);
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteOrderUser.executeClass(order);
			responseObject.setMessage("Delete Order Succesfully");
			responseObject.setStatusCode("200");
			responseObject.setObjResponse(order);
		}
		if(opt.equalsIgnoreCase("retrieveOrderUserObj")) {
			CatalogoItem catItem = (CatalogoItem) catalogItemRepositoryImpl.retrieveByKey(order.getCatalogId(), order.getProductId(), order.getItemNumber());
			if(catItem != null) {
				customer.setCatalogItem(catItem);
			}
			AccountHeader accountHeader = (AccountHeader) accountRepositoryImpl.retrieveAccountById(order.getAccountId().toString());
			if(accountHeader != null) {
				customer.setAccount(accountHeader);
			}
			customer.setOrderUser(order);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
			responseObject.setObjResponse(customer);
		}
		return responseObject;
	}
}
