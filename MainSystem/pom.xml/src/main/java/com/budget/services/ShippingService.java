package com.budget.services;

import com.entities.ResponseObject;
import com.entities.Shipping;

public interface ShippingService {
	   public abstract ResponseObject selectShippingOperation(String opt, Shipping shipping);
}
