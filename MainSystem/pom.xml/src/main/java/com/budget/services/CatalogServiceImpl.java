package com.budget.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.Catalogo;
import com.entities.ResponseObject;
import com.main.catalog.AddCatalog;
import com.main.catalog.DeleteCatalog;
import com.main.catalog.UpdateCatalog;

@Service
public class CatalogServiceImpl implements CatalogService {

	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddCatalog addCatalog;
	@Autowired
	private UpdateCatalog updateCatalog;
	@Autowired
	private DeleteCatalog deleteCatalog;
	
	public ResponseObject selectCatalogOperation(String opt, Catalogo catalog) {
		if(opt.equalsIgnoreCase("add")) {
			addCatalog.executeClass(catalog);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("update")) {
			updateCatalog.executeClass(catalog);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteCatalog.executeClass(catalog);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		return responseObject;
	}
}
