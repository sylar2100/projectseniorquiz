package com.budget.services;

import com.entities.ResponseObject;
import com.entities.UserProfile;

public interface UserService {
	   public abstract ResponseObject selectUserOperation(String opt, UserProfile user);
}
