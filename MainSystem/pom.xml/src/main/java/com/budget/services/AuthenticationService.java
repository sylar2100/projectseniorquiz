package com.budget.services;

import com.entities.UserProfile;

public interface AuthenticationService {
	   public abstract boolean authenticationUser(UserProfile userProfile);
}
