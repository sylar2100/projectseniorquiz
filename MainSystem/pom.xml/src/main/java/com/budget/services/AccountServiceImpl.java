package com.budget.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountLineRepositoryImpl;
import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.entities.ResponseObject;
import com.main.account.AddAccount;
import com.main.account.DeleteAccount;
import com.main.account.UpdateAccount;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AccountLineRepositoryImpl accountLineRepositoryImpl;
	@Autowired
	private AddAccount addAccount;
	@Autowired
	private UpdateAccount updateAccount;
	@Autowired
	private DeleteAccount deleteAccount;
	@Autowired
	private CalculateServiceImpl calculateServiceImpl;

	public ResponseObject selectAccountOperation(String opt, AccountHeader account) {
		if(opt.equalsIgnoreCase("add")) {
			addAccount.executeClass(account);
			responseObject.setMessage("Add Account Succesfully");
			responseObject.setStatusCode("200");
			responseObject.setObjResponse(account);
		}
		if(opt.equalsIgnoreCase("update")) {
			updateAccount.executeClass(account);
			responseObject.setMessage("Update Account Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteAccount.executeClass(account);
			responseObject.setMessage("Delete Account Succesfully");
			responseObject.setStatusCode("200");
			List<Object> accLineList = accountLineRepositoryImpl.retrieveAccountLineById(account.getIcHeader().toString());
			if(accLineList != null && accLineList.size() > 0) {
				for(int al = 0; al < accLineList.size(); al ++) {
					AccountLine accLine = (AccountLine) accLineList.get(al);
					this.selectAccountLineOperation("delete", accLine);
				}
			}
		}
		return responseObject;
	}
	
	public ResponseObject selectAccountLineOperation(String opt, AccountLine account) {
		if(account != null) {
			if(opt.equalsIgnoreCase("add")) {
				addAccount.executeSubClass(account);
				responseObject.setMessage("Add Line Succesfully");
				responseObject.setStatusCode("200");
			}
			if(opt.equalsIgnoreCase("update")) {
				updateAccount.executeSubClass(account);
				responseObject.setMessage("Update Line Succesfully");
				responseObject.setStatusCode("200");
			}
			if(opt.equalsIgnoreCase("delete")) {
				deleteAccount.executeSubClass(account);
				responseObject.setMessage("Delete Line Succesfully");
				responseObject.setStatusCode("200");
			}
			if(account.isCalculateAmount()) {
				calculateServiceImpl.updateBudgetInfo(account);
				calculateServiceImpl.updateCapitalInfo(account);
			}
			if(account.isReplaceAmount()) {
				
			}
		}
		return responseObject;
	}
}
