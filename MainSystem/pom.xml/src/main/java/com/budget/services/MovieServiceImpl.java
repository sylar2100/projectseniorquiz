package com.budget.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.Movies;
import com.entities.ResponseObject;
import com.main.movie.AddMovie;
import com.main.movie.DeleteMovie;
import com.main.movie.UpdateMovie;

@Service
public class MovieServiceImpl implements MovieService {
	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddMovie addMovie;
	@Autowired
	private UpdateMovie updateMovie;
	@Autowired
	private DeleteMovie deleteMovie;
	
	public ResponseObject selectMovieOperation(String opt, Movies movie) {
		if(opt.equalsIgnoreCase("add")) {
			addMovie.executeClass(movie);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("update")) {
			updateMovie.executeClass(movie);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteMovie.executeClass(movie);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		return responseObject;
	}
}
