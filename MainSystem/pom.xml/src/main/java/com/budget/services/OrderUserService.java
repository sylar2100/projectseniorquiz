package com.budget.services;

import com.entities.OrderUser;
import com.entities.ResponseObject;

public interface OrderUserService {
	   public abstract ResponseObject selectOrderUserOperation(String opt, OrderUser order);
}
