package com.budget.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.Budget;
import com.entities.ResponseObject;
import com.main.budget.AddBudget;
import com.main.budget.DeleteBudget;
import com.main.budget.UpdateBudget;

@Service
public class BudgetServiceImpl implements BudgetService {
	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddBudget addBudget;
	@Autowired
	private UpdateBudget updateBudget;
	@Autowired
	private DeleteBudget deleteBudget;
	
	public ResponseObject selectBudgetOperation(String opt, Budget budget) {
		if(opt.equalsIgnoreCase("add")) {
			addBudget.executeClass(budget);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("update")) {
			updateBudget.executeClass(budget);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteBudget.executeClass(budget);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		return responseObject;
	}
}
