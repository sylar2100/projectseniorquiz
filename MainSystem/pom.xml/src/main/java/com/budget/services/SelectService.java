package com.budget.services;

import com.entities.Customer;
import com.entities.ResponseObject;

public interface SelectService {
	public abstract ResponseObject selectService(Customer customer);
}
