package com.budget.services;

import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.entities.ResponseObject;

public interface AccountService {
	   public abstract ResponseObject selectAccountOperation(String opt, AccountHeader accountHeader);
	   public abstract ResponseObject selectAccountLineOperation(String opt, AccountLine accountLine);
}
