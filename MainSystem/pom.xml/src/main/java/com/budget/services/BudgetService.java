package com.budget.services;

import com.entities.Budget;
import com.entities.ResponseObject;

public interface BudgetService {
	   public abstract ResponseObject selectBudgetOperation(String opt, Budget budget);
}
