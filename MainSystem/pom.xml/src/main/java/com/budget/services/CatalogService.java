package com.budget.services;

import com.entities.Catalogo;
import com.entities.ResponseObject;

public interface CatalogService {
	   public abstract ResponseObject selectCatalogOperation(String opt, Catalogo catalog);
}
