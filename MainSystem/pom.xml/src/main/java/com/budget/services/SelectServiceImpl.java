package com.budget.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountLineRepositoryImpl;
import com.budget.repository.AccountRepositoryImpl;
import com.budget.repository.BudgetRepositoryImpl;
import com.budget.repository.CapitalRepositoryImpl;
import com.budget.repository.CatalogItemRepositoryImpl;
import com.budget.repository.CatalogRepositoryImpl;
import com.budget.repository.MovieRepositoryImpl;
import com.budget.repository.OrderUserRepositoryImpl;
import com.budget.repository.ShippingRepositoryImpl;
import com.budget.repository.UserProfileRepositoryImpl;
import com.common.HiltonUtility;
import com.entities.BrowseObjectResponse;
import com.entities.Customer;
import com.entities.Doubt;
import com.entities.ResponseObject;
import com.entities.UserProfile;
import com.exceptions.LoginExceptions;
import com.main.userProfile.UserProfileInit;

@Service
public class SelectServiceImpl implements SelectService {

	@Autowired
    private AuthenticationServiceImpl authServiceImpl;
	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private BrowseObjectResponse browseObjectResponse;
	@Autowired
	private MovieRepositoryImpl movieRepositoryImpl;
	@Autowired
	private MovieServiceImpl movieServiceImpl;
	@Autowired
	private BudgetRepositoryImpl budgetRepositoryImpl;
	@Autowired
	private BudgetServiceImpl budgetServiceImpl;
	@Autowired
	private CapitalRepositoryImpl capitalRepositoryImpl;
	@Autowired
	private CapitalServiceImpl capitalServiceImpl;
	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private CatalogRepositoryImpl catalogRepositoryImpl;
	@Autowired
	private CatalogServiceImpl catalogServiceImpl;
	@Autowired
	private CatalogItemRepositoryImpl catalogItemRepositoryImpl;
	@Autowired
	private CatalogItemServiceImpl catalogItemServiceImpl;
	@Autowired
	private AccountRepositoryImpl accountRepositoryImpl;
	@Autowired
	private AccountServiceImpl accountServiceImpl;
	@Autowired
	private ShippingRepositoryImpl shippingRepositoryImpl;
	@Autowired
	private ShippingServiceImpl shippingServiceImpl;
	@Autowired
	private OrderUserRepositoryImpl orderUserRepositoryImpl;
	@Autowired
	private OrderUserServiceImpl orderUserServiceImpl;
	@Autowired
	private AccountLineRepositoryImpl accountLineRepositoryImpl;
	@Autowired
	private UserProfileRepositoryImpl userProfileRepositoryImpl;
	@Autowired
	public UserProfileInit userProfileInit;
	@Autowired
	public InitServiceData initServiceData;
	
	public ResponseObject selectService(Customer customer) {
		String handler = HiltonUtility.ckNull(customer.getHandler());
		String opt = HiltonUtility.ckNull(customer.getOperation());
		
		if(handler.equalsIgnoreCase("userProfile")) {
			if(opt.equalsIgnoreCase("authentication") && customer.getUser() != null) {
				UserProfile userProfile = customer.getUser();
				try {
					if(authServiceImpl.authenticationUser(userProfile)) {
						responseObject.setMessage("Login Succesfully");
						responseObject.setStatusCode("200");
					} else {
						throw new LoginExceptions(1);
					}
				} catch (LoginExceptions ex) {
		            responseObject.setMessage(ex.getMessage());
		            responseObject.setStatusCode("1");
		        }
			} 
		} 
		if(handler.equalsIgnoreCase("movie") && customer.getMovies() != null) {
			responseObject = movieServiceImpl.selectMovieOperation(opt, customer.getMovies());
		}
		if(handler.equalsIgnoreCase("user") && customer.getUser() != null) {
			responseObject = userServiceImpl.selectUserOperation(opt, customer.getUser());
			userProfileInit.cleanList();
		}
		if(handler.equalsIgnoreCase("catalog") && customer.getCatalog() != null) {
			responseObject = catalogServiceImpl.selectCatalogOperation(opt, customer.getCatalog());
		}
		if(handler.equalsIgnoreCase("catalogItem") && customer.getCatalogItem() != null) {
			responseObject = catalogItemServiceImpl.selectCatalogItemOperation(opt, customer.getCatalogItem());
		}
		if(handler.equalsIgnoreCase("account") && customer.getAccount() != null) {
			responseObject = accountServiceImpl.selectAccountOperation(opt, customer.getAccount());
		}
		if(handler.equalsIgnoreCase("accountLine") && customer.getAccountLine() != null) {
			responseObject = accountServiceImpl.selectAccountLineOperation(opt, customer.getAccountLine());
		}
		if(handler.equalsIgnoreCase("shipping") && customer.getShipping() != null) {
			responseObject = shippingServiceImpl.selectShippingOperation(opt, customer.getShipping());
		}
		if(handler.equalsIgnoreCase("budget") && customer.getBudget() != null) {
			responseObject = budgetServiceImpl.selectBudgetOperation(opt, customer.getBudget());
		}
		if(handler.equalsIgnoreCase("capital") && customer.getCapital() != null) {
			responseObject = capitalServiceImpl.selectCapitalOperation(opt, customer.getCapital());
		}
		if(handler.equalsIgnoreCase("orderUser") && customer.getOrderUser() != null) {
			responseObject = orderUserServiceImpl.selectOrderUserOperation(opt, customer.getOrderUser());
		}
		if(handler.equalsIgnoreCase("dataInit")) {
			responseObject.setUserProfileList(userProfileInit.getInstance());
			responseObject.setMapData(initServiceData.getInstance());
		}
		return responseObject;
	}
	
	public BrowseObjectResponse selectBrowse(Customer customer) {
		String handler = HiltonUtility.ckNull(customer.getHandler());
		String opt = HiltonUtility.ckNull(customer.getOperation());
		List<String> movieLabels = Arrays.asList("Title","Spanish Title", "Device", "Ranking", "Image");
		List<String> movieColumns = Arrays.asList("originalTitle","title", "device", "ranking", "imageMovie");
		List<String> catalogLabels = Arrays.asList("Catalog","Title","Description", "Status", "Image");
		List<String> catalogColumns = Arrays.asList("catalogId","title","description", "status", "catalogoImage");
		List<String> catalogItemLabels = Arrays.asList("Catalog","Product","Description", "Vendor", "Buyer", "Version","Image");
		List<String> catalogItemColumns = Arrays.asList("id.catalogId","id.productId","description","vendorId","buyerId","version","imageFile");
		List<String> accountLabels = Arrays.asList("Description","Date","User", "Status");
		List<String> accountColumns = Arrays.asList("description","accountDate","userId", "status");
		List<String> accountLineLabels = Arrays.asList("Income","Expenses","PayPal Transaction", "PayPal Email", "Capital", "Budget");
		List<String> accountLineColumns = Arrays.asList("income","expenses","paypalTransaction", "paypalEmail", "capitalId","budgetId");
		List<String> userLabels = Arrays.asList("User","Mail","First Name","Last Name", "City", "Country");
		List<String> userColumns = Arrays.asList("userId","mailId","firstName","lastName", "city", "country");
		List<String> budgetLabels = Arrays.asList("Description","Amount","Balance","Period", "Currency");
		List<String> budgetColumns = Arrays.asList("description","amount","balance","budgetPeriod", "currencyCode");
		List<String> capitalLabels = Arrays.asList("Description","Currency","AMOUNT","Class Capital");
		List<String> capitalColumns = Arrays.asList("description","currencyCode","amount","classCapital");
		List<String> shipLabels = Arrays.asList("Catalog","Product","Postal Service","Tracking Number");
		List<String> shipColumns = Arrays.asList("catalogId","productId","postalService","trackingNumber");
		List<String> doubtLabels = Arrays.asList("User","Description","Version","Cost","Sales Cost", "Paid Amount", "Paid Send", "Doubt", "Dollar Cost");
		List<String> doubtColumns = Arrays.asList("userId","description","version","cost","salesCost", "paidAmount", "paidSend", "doubt", "dollarCost");
		List<String> orderLabels = Arrays.asList("User","Catalog","Product","Order Number","Status","Paid Amount", "Paid Send");
		List<String> orderColumns = Arrays.asList("userId","catalogId","productId","poNumber","status", "paidAmount", "paidSend");
		
		if(handler.equalsIgnoreCase("movie")) {
			if(opt.equalsIgnoreCase("allMovies")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(movieLabels);
				browseObjectResponse.setColumns(movieColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(movieRepositoryImpl.findAll());
				browseObjectResponse.setImagePath("images/movies/");
			} 
		} 
		if(handler.equalsIgnoreCase("catalog")) {
			if(opt.equalsIgnoreCase("allCatalogs")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(catalogLabels);
				browseObjectResponse.setColumns(catalogColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(catalogRepositoryImpl.findAll());
				browseObjectResponse.setImagePath("images/catalogs/");
			} 
		} 
		if(handler.equalsIgnoreCase("orderUser")) {	
			if(opt.equalsIgnoreCase("allOrderUser")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(orderLabels);
				browseObjectResponse.setColumns(orderColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(orderUserRepositoryImpl.findAll());
			} 
			if(opt.equalsIgnoreCase("madeOrder")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(orderLabels);
				browseObjectResponse.setColumns(orderColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(orderUserRepositoryImpl.madeOrderRetrieve());
			}
			if(opt.equalsIgnoreCase("preOrdenPartiallyPaid")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(orderLabels);
				browseObjectResponse.setColumns(orderColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(orderUserRepositoryImpl.preOrdenPartiallyPaid());
			}
			if(opt.equalsIgnoreCase("preOrdenStock")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(orderLabels);
				browseObjectResponse.setColumns(orderColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(orderUserRepositoryImpl.preOrdenInStock());
			}
			if(opt.equalsIgnoreCase("OrdenStock")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(orderLabels);
				browseObjectResponse.setColumns(orderColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(orderUserRepositoryImpl.ordenInStock());
			}
			
			
		} 
		if(handler.equalsIgnoreCase("catalogItem")) {
			if(opt.equalsIgnoreCase("allCatalogItemsTemplate")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(catalogItemLabels);
				browseObjectResponse.setColumns(catalogItemColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(catalogItemRepositoryImpl.findAllTemplate());
				browseObjectResponse.setImagePath("images/catalogs/catalogItems/");
			} 
		}
		if(handler.equalsIgnoreCase("shipping")) {
			if(opt.equalsIgnoreCase("shippingFromForeign")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(catalogItemLabels);
				browseObjectResponse.setColumns(catalogItemColumns);
				browseObjectResponse.setNeedButtons(false);
				browseObjectResponse.setViewButton(true);
				browseObjectResponse.setObjResponse(catalogItemRepositoryImpl.retrieveShippingFromForeign("05"));
				browseObjectResponse.setImagePath("images/catalogs/catalogItems/");
			}
			if(opt.equalsIgnoreCase("arriveItem")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(shipLabels);
				browseObjectResponse.setColumns(shipColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(shippingRepositoryImpl.retrieveArriveItems("06"));
			}
			if(opt.equalsIgnoreCase("delivered")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(shipLabels);
				browseObjectResponse.setColumns(shipColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(shippingRepositoryImpl.retrieveDeliveredItems("08"));
			}
			if(opt.equalsIgnoreCase("readyForDelivery")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(orderLabels);
				browseObjectResponse.setColumns(orderColumns);
				browseObjectResponse.setNeedButtons(false);
				browseObjectResponse.setViewButton(true);
				browseObjectResponse.setObjResponse(orderUserRepositoryImpl.retrieveReadyforDelivery());
				browseObjectResponse.setImagePath("images/catalogs/catalogItems/");
			}
		} 
		if(handler.equalsIgnoreCase("user")) {
			if(opt.equalsIgnoreCase("allUsers")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(userLabels);
				browseObjectResponse.setColumns(userColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				List<Object> newList = new ArrayList<Object>();
				newList.addAll(userProfileInit.getInstance());
				browseObjectResponse.setObjResponse(newList);
			} 
		} 
		if(handler.equalsIgnoreCase("budget")) {
			if(opt.equalsIgnoreCase("allBudgets")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(budgetLabels);
				browseObjectResponse.setColumns(budgetColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(budgetRepositoryImpl.findAll());
			} 
		} 
		if(handler.equalsIgnoreCase("capital")) {
			if(opt.equalsIgnoreCase("allCapitals")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(capitalLabels);
				browseObjectResponse.setColumns(capitalColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(capitalRepositoryImpl.findAll());
			} 
		} 
		if(handler.equalsIgnoreCase("account")) {
			if(opt.equalsIgnoreCase("allAccounts")) {
				browseObjectResponse.setMessage("Return Data Succesfully");
				browseObjectResponse.setStatusCode("200");
				browseObjectResponse.setLabels(accountLabels);
				browseObjectResponse.setColumns(accountColumns);
				browseObjectResponse.setNeedButtons(true);
				browseObjectResponse.setViewButton(false);
				browseObjectResponse.setObjResponse(accountRepositoryImpl.findAll());
			} 
		} 
		if(handler.equalsIgnoreCase("doubt")) {
			List<Object> doubtList = new ArrayList<Object>();
			browseObjectResponse.setMessage("Return Data Succesfully");
			browseObjectResponse.setStatusCode("200");
			browseObjectResponse.setLabels(doubtLabels);
			browseObjectResponse.setColumns(doubtColumns);
			browseObjectResponse.setViewButton(false);
			browseObjectResponse.setNeedButtons(false);
			List<Object> resultList = new ArrayList<Object>();
			if(opt.equalsIgnoreCase("doubt")) {
				resultList = orderUserRepositoryImpl.retrieveDoubt(); 
			} else if(opt.equalsIgnoreCase("orderUserDeliveryByPendingPay")) {
				resultList = orderUserRepositoryImpl.orderUserDeliveryByPendingPay(); 
			} else if(opt.equalsIgnoreCase("doubtOrderUserStock")) {
				resultList = orderUserRepositoryImpl.doubtOrderUserStock(); 
			}
			if(resultList != null && resultList.size() > 0){
				for(Object data : resultList) {
					Doubt doubt = new Doubt();
					Object[] dataPopulate = (Object[]) data;
					doubt.setUserId((String) dataPopulate[0]);
					doubt.setDescription((String) dataPopulate[1]);
					doubt.setVersion((String) dataPopulate[2]);
					doubt.setCost((BigDecimal) dataPopulate[3]);
					doubt.setSalesCost((BigDecimal) dataPopulate[4]);
					doubt.setPaidAmount((BigDecimal) dataPopulate[5]);
					doubt.setPaidSend((BigDecimal) dataPopulate[6]);
					doubt.setDoubt((BigDecimal) dataPopulate[7]);
					doubt.setDollarCost((Double) dataPopulate[8]);
					doubtList.add(doubt);
				}
			}
			browseObjectResponse.setObjResponse(doubtList);
		} 
		if(handler.equalsIgnoreCase("account")) {
			if(opt.equalsIgnoreCase("retrieveAccountLineById") && customer.getAccount() != null) {			
				List<Object> objList = null;
				objList = accountLineRepositoryImpl.retrieveAccountLineById(customer.getAccount().getIcHeader().toString());
				if(objList.size() > 0) {
					browseObjectResponse.setMessage("Return Data Succesfully");
					browseObjectResponse.setStatusCode("200");
					browseObjectResponse.setLabels(accountLineLabels);
					browseObjectResponse.setColumns(accountLineColumns);
					browseObjectResponse.setNeedButtons(true);
					browseObjectResponse.setViewButton(false);
					browseObjectResponse.setObjResponse(objList);
				} else {
					responseObject.setMessage("No Records Found");
					responseObject.setStatusCode("10");
					browseObjectResponse.setObjResponse(objList);
				}
			} 
		} 
		return browseObjectResponse;
	}
	
	public BrowseObjectResponse getParticipants() {
		browseObjectResponse.setObjResponse(userProfileRepositoryImpl.sorteo());
		return browseObjectResponse;
	}
}
