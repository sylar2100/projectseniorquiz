package com.budget.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.BudgetRepositoryImpl;
import com.budget.repository.CatalogItemRepositoryImpl;
import com.budget.repository.OrderUserRepositoryImpl;
import com.budget.repository.ShippingRepositoryImpl;
import com.common.DocumentStatus;
import com.common.HiltonUtility;
import com.entities.Budget;
import com.entities.CatalogoItem;
import com.entities.Currency;
import com.entities.OrderUser;
import com.entities.ResponseObject;
import com.entities.Shipping;
import com.main.catalogItem.UpdateCatalogItem;
import com.main.currency.SearchCurrency;
import com.main.orderuser.UpdateOrderUser;
import com.main.shipping.AddShipping;
import com.main.shipping.DeleteShipping;
import com.main.shipping.UpdateShipping;

@Service
public class ShippingServiceImpl implements ShippingService {

	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddShipping addShipping;
	@Autowired
	private UpdateShipping updateShipping;
	@Autowired
	private DeleteShipping deleteShipping;
	@Autowired
	private BudgetRepositoryImpl budgetRepositoryImpl;
	@Autowired
	private CatalogItemRepositoryImpl catalogItemRepositoryImpl;
	@Autowired
	private OrderUserRepositoryImpl orderUserRepositoryImpl;
	@Autowired
	private ShippingRepositoryImpl shippingRepositoryImpl;
	@Autowired
	private UpdateCatalogItem updateCatalogItem;
	@Autowired
	private UpdateOrderUser updateOrderUser;
	@Autowired
	private CalculateServiceImpl calculateServiceImpl;

	public ResponseObject selectShippingOperation(String opt, Shipping shipping) {
		if(shipping != null) {
			String action = HiltonUtility.ckNull(shipping.getAction());
			if(opt.equalsIgnoreCase("add")) {
				addShipping.executeProcess(shipping);
				responseObject.setMessage("Add Shipping Succesfully");
				responseObject.setStatusCode("100");
			}
			if(opt.equalsIgnoreCase("update")) {
				updateShipping.executeClass(shipping);
				responseObject.setMessage("Update Shipping Succesfully");
				responseObject.setStatusCode("110");
			}
			if(opt.equalsIgnoreCase("delete")) {
				deleteShipping.executeClass(shipping);
				responseObject.setMessage("Delete Shipping Succesfully");
				responseObject.setStatusCode("200");
			}
			if(action.equalsIgnoreCase("SF")) {
				CatalogoItem catalogoItem = (CatalogoItem) catalogItemRepositoryImpl.retrieveByKey(shipping.getCatalogId(), shipping.getProductId(), shipping.getItemNumber());
				catalogoItem.setStatus("06");
				updateCatalogItem.executeClass(catalogoItem);
			}
			if(action.equalsIgnoreCase("A")) {
				List<Object> queryResult = shippingRepositoryImpl.searchShippingTracking(shipping.getTrackingNumber());
				if(queryResult.size() > 0) {
					for(int s = 0; s < queryResult.size(); s++) {
						shipping = (Shipping) queryResult.get(s);
						CatalogoItem catalogoItem = (CatalogoItem) catalogItemRepositoryImpl.retrieveByKey(shipping.getCatalogId(), shipping.getProductId(), shipping.getItemNumber());
						OrderUser orderUser = (OrderUser) orderUserRepositoryImpl.retrieveByKey(shipping.getCatalogId(), shipping.getProductId(), shipping.getItemNumber());
						if(catalogoItem != null) {
							catalogoItem.setStatus("07"); 
							updateCatalogItem.executeClass(catalogoItem);
						}
						if(orderUser != null) {
							orderUser.setStatus("11");
							updateOrderUser.executeClass(orderUser);
						}
					}
				}
			}
			if(action.equalsIgnoreCase("C")) {
				CatalogoItem catalogoItem = (CatalogoItem) catalogItemRepositoryImpl.retrieveByKey(shipping.getCatalogId(), shipping.getProductId(), shipping.getItemNumber());
				catalogoItem.setStatus("08");
				updateCatalogItem.executeClass(catalogoItem);		
			}
			if(action.equalsIgnoreCase("D")) {
				CatalogoItem catalogoItem = (CatalogoItem) catalogItemRepositoryImpl.retrieveByKey(shipping.getCatalogId(), shipping.getProductId(), shipping.getItemNumber());
				catalogoItem.setStatus("09");
				updateCatalogItem.executeClass(catalogoItem);	
				String status = DocumentStatus.Completed.getKey();
				if(status.equals(catalogoItem.getStatus())) {
					BigDecimal gain = catalogoItem.getSalesCost().subtract(catalogoItem.getCost());
					BigDecimal buyDollars = BigDecimal.ZERO;
					Currency currency = (Currency) SearchCurrency.getInstance().searchById("USD");
					if(currency != null) {
						buyDollars = catalogoItem.getCost().divide(currency.getCurrencyChange(),RoundingMode.HALF_UP);
					}
					Budget budgetGain = (Budget) budgetRepositoryImpl.retrieveById("50453443996600");
					Budget budgetBuyedDollars = (Budget) budgetRepositoryImpl.retrieveById("50453443996700");
					calculateServiceImpl.updateBudgetAmount(budgetGain, gain, "Y", true);
					calculateServiceImpl.updateBudgetAmount(budgetBuyedDollars, buyDollars, "Y", true);
				}
			}
		}
		return responseObject;
	}
}
