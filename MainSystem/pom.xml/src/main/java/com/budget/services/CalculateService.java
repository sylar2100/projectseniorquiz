package com.budget.services;

import com.entities.AccountLine;

public interface CalculateService {
	public abstract void updateBudgetInfo(AccountLine accLine);
	public abstract void updateCapitalInfo(AccountLine accLine);
}
