package com.budget.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountLineRepositoryImpl;
import com.budget.repository.BudgetRepositoryImpl;
import com.common.HiltonUtility;
import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.entities.Budget;
import com.entities.Capital;
import com.entities.Currency;
import com.entities.ResponseObject;
import com.main.account.AddAccount;
import com.main.account.DeleteAccount;
import com.main.account.UpdateAccount;
import com.main.budget.SearchBudget;
import com.main.budget.UpdateBudget;
import com.main.capital.SearchCapital;
import com.main.capital.UpdateCapital;
import com.main.currency.SearchCurrency;

@Service
public class CalculateServiceImpl implements CalculateService {

	@Autowired 
	public BudgetRepositoryImpl budgetRepositoryImpl;
	public void updateBudgetInfo(AccountLine accLine) {		
		BigDecimal incomeAmount = HiltonUtility.ckNull(accLine.getIncome());
		BigDecimal expenseAmount = HiltonUtility.ckNull(accLine.getExpenses());
		if(accLine.getBudgetId().compareTo(BigDecimal.ZERO) != 0) {
			Budget budget = (Budget) SearchBudget.getInstance().retrieveById(accLine.getBudgetId());
			if(!budget.getCurrencyCode().equalsIgnoreCase(accLine.getCurrencyCode())) {
				Currency result = (Currency) SearchCurrency.getInstance().searchById(accLine.getCurrencyCode());
				if(result != null) {
					incomeAmount = incomeAmount.multiply(result.getCurrencyChange());
					expenseAmount = expenseAmount.multiply(result.getCurrencyChange());
				}
			}
			if(incomeAmount.compareTo(BigDecimal.ZERO) > 0) {
				UpdateBudget.getInstance().updateBudgetAmount(budget, incomeAmount, "Y", accLine.isReplaceAmount());
			}
			if(expenseAmount.compareTo(BigDecimal.ZERO) > 0) {
				UpdateBudget.getInstance().updateBudgetAmount(budget, expenseAmount, "N", accLine.isReplaceAmount());
			}
		} 
	}
	
	public void updateCapitalInfo(AccountLine accLine) {	
		BigDecimal incomeAmount = HiltonUtility.ckNull(accLine.getIncome());
		BigDecimal expenseAmount = HiltonUtility.ckNull(accLine.getExpenses());
		if(!accLine.getCapitalId().equalsIgnoreCase("0")) {
			Capital capital = (Capital) SearchCapital.getInstance().retrieveById(accLine.getCapitalId());
			if(!capital.getCurrencyCode().equalsIgnoreCase(accLine.getCurrencyCode())) {
				Currency result = (Currency) SearchCurrency.getInstance().searchById(accLine.getCurrencyCode());
				if(result != null) {
					incomeAmount = incomeAmount.multiply(result.getCurrencyChange());
					expenseAmount = expenseAmount.multiply(result.getCurrencyChange());
				}
			}
			if(incomeAmount.compareTo(BigDecimal.ZERO) > 0) {
				UpdateCapital.getInstance().updateCapitalAmount(capital, incomeAmount, "Y");
			}
			if(expenseAmount.compareTo(BigDecimal.ZERO) > 0) {
				UpdateCapital.getInstance().updateCapitalAmount(capital, expenseAmount, "N");
			}
		} 
	}
	
	public void updateBudgetAmount(Budget budget, BigDecimal amount, String add, boolean replaceAmount) {
		String message = "";
		if(budget != null) {
			if(replaceAmount) {
				if(add.equalsIgnoreCase("Y")) {
					budget.setAmount(budget.getAmount().add(amount));
				} else {
					budget.setAmount(budget.getAmount().subtract(amount));
				}
			}
			if(add.equalsIgnoreCase("Y")) {
				budget.setBalance(budget.getBalance().add(amount));
			} else {
				budget.setBalance(budget.getBalance().subtract(amount));
			}
			budgetRepositoryImpl.updateBudget(budget);
		}
	}
}
