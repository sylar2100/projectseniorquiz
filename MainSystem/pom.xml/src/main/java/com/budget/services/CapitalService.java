package com.budget.services;

import com.entities.Capital;
import com.entities.ResponseObject;

public interface CapitalService {
	   public abstract ResponseObject selectCapitalOperation(String opt, Capital capital);
}
