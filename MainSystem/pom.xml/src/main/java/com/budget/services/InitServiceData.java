package com.budget.services;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.BudgetRepositoryImpl;
import com.budget.repository.CapitalRepositoryImpl;
import com.budget.repository.CatalogRepositoryImpl;
import com.budget.repository.CurrencyRepositoryImpl;
import com.budget.repository.VendorRepositoryImpl;
import com.common.DocumentStatus;
import com.common.PayPalEmails;
import com.entities.Budget;
import com.entities.Capital;
import com.entities.Catalogo;
import com.entities.Currency;
import com.entities.Vendor;

@Service
public class InitServiceData {
	@Autowired
	public CurrencyRepositoryImpl currencyRepositoryImpl;
	@Autowired
	public CapitalRepositoryImpl capitalRepositoryImpl;
	@Autowired
	public BudgetRepositoryImpl budgetRepositoryImpl;
	@Autowired
	public CatalogRepositoryImpl catalogRepositoryImpl;
	@Autowired
	public VendorRepositoryImpl vendorRepositoryImpl;
	public static HashMap<String,HashMap<String,String>> mapData = new HashMap<String,HashMap<String,String>>();
	
	public HashMap<String,HashMap<String,String>> getInstance() {
		if(mapData.isEmpty()) {
			this.loadAll();
		}
		return mapData;
	}
	public void loadAll() {
		List<Object> listCurrs = currencyRepositoryImpl.findAll();
		List<Object> listCapitals = capitalRepositoryImpl.retrieveByClassB();
		List<Object> listBudgets = budgetRepositoryImpl.findAll();
		List<Object> listCatalogs = catalogRepositoryImpl.findAll();
		List<Object> listVendors = vendorRepositoryImpl.findAll();
		LinkedHashMap<String,String> entityDataCurr = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> entityDataCap = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> entityDataBudget = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> entityDataStatus = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> entityDataEmails = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> entityDataCatalogs = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> entityDataVendors = new LinkedHashMap<String,String>();
		if(listCurrs != null && listCurrs.size() > 0) {
			listCurrs.forEach(curr -> {
				Currency currency = (Currency) curr;
				entityDataCurr.put(currency.getCurrencyCode(), currency.getCurrencyName());	
			});
			mapData.put("currency", entityDataCurr);
		}
		if(listCapitals != null && listCapitals.size() > 0) {
			listCapitals.forEach(cap -> {
				Capital capital = (Capital) cap;
				entityDataCap.put(capital.getCapitalId(), capital.getDescription());
			});
			mapData.put("capital", entityDataCap);
		}
		if(listBudgets != null && listBudgets.size() > 0) {
			listBudgets.forEach(budget -> {
				Budget budgetObj = (Budget) budget;
				entityDataBudget.put(budgetObj.getBudgetId().toString(), budgetObj.getDescription());
			});
			mapData.put("budget", entityDataBudget);
		}
		if(listCatalogs != null && listCatalogs.size() > 0) {
			listCatalogs.forEach(catalog -> {
				Catalogo catalogObj = (Catalogo) catalog;
				entityDataCatalogs.put(catalogObj.getCatalogId(), catalogObj.getTitle());
			});
			mapData.put("catalog", entityDataCatalogs);
		}
		if(listVendors != null && listVendors.size() > 0) {
			listVendors.forEach(vendor -> {
				Vendor vendorObj = (Vendor) vendor;
				entityDataVendors.put(vendorObj.getVendorId(), vendorObj.getStoreName());
			});
			mapData.put("vendor", entityDataVendors);
		}
		for (DocumentStatus status : DocumentStatus.values()) {
			entityDataStatus.put(status.getKey(), status.getValue());
			mapData.put("statusList", entityDataStatus);
		}
		for (PayPalEmails email : PayPalEmails.values()) {
			entityDataEmails.put(email.getStatusName(), email.getStatusName());
			mapData.put("emailsList", entityDataEmails);
		}
	} 
	public void cleanList() {
		mapData.clear();
	}
}

