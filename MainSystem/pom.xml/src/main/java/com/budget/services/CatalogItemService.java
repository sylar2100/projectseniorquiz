package com.budget.services;

import com.entities.CatalogoItem;
import com.entities.ResponseObject;

public interface CatalogItemService {
	   public abstract ResponseObject selectCatalogItemOperation(String opt, CatalogoItem catalogItem);
}
