package com.budget.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.UserProfileRepositoryImpl;
import com.common.HiltonUtility;
import com.entities.UserProfile;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
    private UserProfileRepositoryImpl userProfileRepository;
	public boolean authenticationUser(UserProfile userProfile) {
		boolean loginUser = false;
		String userId = HiltonUtility.ckNull(userProfile.getUserId());
		String userPassword = HiltonUtility.ckNull(userProfile.getUserPassword());
		UserProfile authUser = (UserProfile) userProfileRepository.findByUserIdAndPass(userId, userPassword);
		if(authUser != null) {
			loginUser = true;
		}
		return loginUser;
	}
}
