package com.budget.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.Capital;
import com.entities.ResponseObject;
import com.main.capital.AddCapital;
import com.main.capital.DeleteCapital;
import com.main.capital.UpdateCapital;

@Service
public class CapitalServiceImpl implements CapitalService {
	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddCapital addCapital;
	@Autowired
	private UpdateCapital updateCapital;
	@Autowired
	private DeleteCapital deleteCapital;
	
	public ResponseObject selectCapitalOperation(String opt, Capital capital) {
		if(opt.equalsIgnoreCase("add")) {
			addCapital.executeClass(capital);
			responseObject.setMessage("Add Capital Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("update")) {
			updateCapital.executeClass(capital);
			responseObject.setMessage("Update Capital Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteCapital.executeClass(capital);
			responseObject.setMessage("Delete Capital Succesfully");
			responseObject.setStatusCode("200");
		}
		return responseObject;
	}
}
