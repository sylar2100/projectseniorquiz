package com.budget.services;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.BudgetRepositoryImpl;
import com.common.DocumentStatus;
import com.entities.Budget;
import com.entities.CatalogoItem;
import com.entities.Currency;
import com.entities.ResponseObject;
import com.main.catalogItem.AddCatalogItem;
import com.main.catalogItem.DeleteCatalogItem;
import com.main.catalogItem.UpdateCatalogItem;
import com.main.currency.SearchCurrency;

@Service
public class CatalogItemServiceImpl implements CatalogItemService {

	@Autowired
	private ResponseObject responseObject;
	@Autowired
	private AddCatalogItem addCatalog;
	@Autowired
	private BudgetRepositoryImpl budgetRepositoryImpl;
	@Autowired
	private CalculateServiceImpl calculateServiceImpl;
	@Autowired
	private UpdateCatalogItem updateCatalog;
	@Autowired
	private DeleteCatalogItem deleteCatalog;
	
	public ResponseObject selectCatalogItemOperation(String opt, CatalogoItem catalogItem) {
		if(opt.equalsIgnoreCase("add")) {
			addCatalog.executeClass(catalogItem);
			responseObject.setMessage("Add Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("update")) {
			updateCatalog.executeClass(catalogItem);
			responseObject.setMessage("Update Succesfully");
			responseObject.setStatusCode("200");
		}
		if(opt.equalsIgnoreCase("delete")) {
			deleteCatalog.executeClass(catalogItem);
			responseObject.setMessage("Delete Succesfully");
			responseObject.setStatusCode("200");
		}
		responseObject.setObjResponse(catalogItem);
		String status = DocumentStatus.Completed.getKey();
		if(status.equals(catalogItem.getStatus()) && catalogItem.isCalculateItemComplete()) {
			BigDecimal gain = catalogItem.getSalesCost().subtract(catalogItem.getCost());
			BigDecimal buyDollars = BigDecimal.ZERO;
			Currency result = (Currency) SearchCurrency.getInstance().searchById("USD");
			if(result != null) {
				buyDollars = catalogItem.getCost().divide(result.getCurrencyChange(),RoundingMode.HALF_UP);
			}
			Budget budgetGain = (Budget) budgetRepositoryImpl.retrieveById("50453443996600");
			Budget budgetBuyedDollars = (Budget) budgetRepositoryImpl.retrieveById("50453443996700");
			calculateServiceImpl.updateBudgetAmount(budgetGain, gain, "Y", true);
			calculateServiceImpl.updateBudgetAmount(budgetBuyedDollars, buyDollars, "Y", true);
		}
		return responseObject;
	}
}
