package com.budget.repository;

import java.util.List;

import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.transaction.EntityManagerUtilities;

public interface AccountRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public Object retrieveAccountById(String icHeader);
	public void insertAccount(AccountHeader account);
	public void updateAccount(AccountHeader account);
	public void deleteAccount(AccountHeader account);
	public void insertAccountLine(AccountLine account);
	public void updateAccountLine(AccountLine account);
	public void deleteAccountLine(AccountLine account);
}
