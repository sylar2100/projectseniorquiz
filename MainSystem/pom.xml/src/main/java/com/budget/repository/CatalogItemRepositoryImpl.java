package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Catalogo;
import com.entities.CatalogoItem;

@Repository
public class CatalogItemRepositoryImpl implements CatalogItemRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from CatalogoItem order by id.catalogId,id.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public List<Object> findAllTemplate() {
		// TODO Auto-generated method stub
		String query = "from CatalogoItem where template='Y' order by id.catalogId,id.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public Object retrieveByKey(String catId, String prodId, String itemNumber) {
		String query = "from CatalogoItem where id.catalogId = '" + catId + "' and id.productId = '" + prodId + "' and id.itemNumber = '" + itemNumber + "'";
		Object result = null;
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
	
	public List<Object> retrieveByCatalogIdAndProductId(String catId, String prodId) {
		String query = "from CatalogoItem where id.catalogId = '" + catId + "' and id.productId = '" + prodId + "' and template <> 'Y'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveShippingFromForeign(String status) {
		String query = "from CatalogoItem where status='" + status + "' order by id.catalogId,id.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveReadyforDelivery() {
		String query = "from CatalogoItem where status='13' order by id.catalogId,id.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}

	public void insertCatalogoItem(CatalogoItem catalog) {
		entityManagerUtilities.addDB(catalog);	
	}

	public void updateCatalogoItem(CatalogoItem catalog) {
		entityManagerUtilities.updateDB(CatalogoItem.class, catalog, catalog.getId());
	}
	
	public void deleteCatalogoItem(CatalogoItem catalog) {
		entityManagerUtilities.deleteDB(CatalogoItem.class, catalog.getId());
	}

}
