package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Budget;

@Repository
public class BudgetRepositoryImpl implements BudgetRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from Budget where status='01' order by budgetId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public Object retrieveById(String budgetId) {
		// TODO Auto-generated method stub
		String query = "from Budget where status='01' and budgetId = '" + budgetId + "' order by budgetId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		Object result = null;
		if(queryResult != null && queryResult.size() > 0) {
			result = queryResult.get(0);
		}
	
		return result;
	}

	@Override
	public void insertBudget(Budget budget) {
		entityManagerUtilities.addDB(budget);	
	}

	@Override
	public void updateBudget(Budget budget) {
		entityManagerUtilities.updateDB(Budget.class, budget, budget.getBudgetId());	
	}

	@Override
	public void deleteBudget(Budget budget) {
		entityManagerUtilities.deleteDB(Budget.class, budget.getBudgetId());
	}

}
