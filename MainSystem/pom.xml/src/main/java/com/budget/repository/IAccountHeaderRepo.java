package com.budget.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entities.AccountHeader;

public interface IAccountHeaderRepo extends JpaRepository<AccountHeader, BigDecimal> {

}
