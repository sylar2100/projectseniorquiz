package com.budget.repository;

import java.util.List;

import com.entities.OrderUser;
import com.transaction.EntityManagerUtilities;

public interface OrderUserRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public Object retrieveByKey(String catId, String prodId, String itemNumber);
	public List<Object> retrieveDoubt();
	public List<Object> searchOrderByUser(String userId);
	public List<Object> madeOrderRetrieve();
	public List<Object> orderUserDeliveryByPendingPay();
	public List<Object> preOrdenInStock();
	public List<Object> ordenInStock();
	public List<Object> preOrdenPartiallyPaid();
	public List<Object> retrieveReadyforDelivery();
	public void insertOrderUser(OrderUser order);
	public void updateOrderUser(OrderUser order);
	public void deleteOrderUser(OrderUser order);
}
