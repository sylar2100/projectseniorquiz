package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Capital;

@Repository
public class CapitalRepositoryImpl implements CapitalRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from Capital where status='01' order by capitalId, classCapital";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public List<Object> retrieveByClassB() {
		String query = "from Capital where classCapital = 'B' and status = '01' order by description";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);

		return queryResult;
	}
	
	@Override
	public void insertCapital(Capital capital) {
		entityManagerUtilities.addDB(capital);	
	}

	@Override
	public void updateCapital(Capital capital) {
		entityManagerUtilities.updateDB(Capital.class, capital, capital.getCapitalId());	
	}

	@Override
	public void deleteCapital(Capital capital) {
		entityManagerUtilities.deleteDB(Capital.class, capital.getCapitalId());
	}

}
