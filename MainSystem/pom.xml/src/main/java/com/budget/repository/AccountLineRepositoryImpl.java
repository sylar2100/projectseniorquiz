package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.AccountHeader;
import com.entities.AccountLine;

@Repository
public class AccountLineRepositoryImpl implements AccountLineRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from AccountLine order by accountDate desc";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}

	public List<Object> retrieveAccountLineById(String icHeader) {
		// TODO Auto-generated method stub
		String query = "from AccountLine where icHeader = '" + icHeader + "' order by accountDate desc";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}

	public void insertAccount(AccountLine account) {
		entityManagerUtilities.addDB(account);	
	}

	public void updateAccount(AccountLine account) {
		entityManagerUtilities.updateDB(AccountHeader.class, account, account.getIcHeader());
	}
	
	public void deleteAccount(AccountLine account) {
		entityManagerUtilities.deleteDB(AccountHeader.class, account.getIcHeader());
	}

}
