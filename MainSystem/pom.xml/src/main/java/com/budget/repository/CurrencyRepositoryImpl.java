package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class CurrencyRepositoryImpl implements CurrencyRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from Currency order by currencyCode";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}

}
