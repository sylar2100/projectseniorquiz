package com.budget.repository;

import java.util.List;

import com.entities.UserProfile;
import com.transaction.EntityManagerUtilities;

public interface UserProfileRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public Object findByUserIdAndPass(String userId, String userPassword);
	public List<Object> findAll();
	public List<Object> sorteo();
	public void insertUser(UserProfile user);
	public void updateUser(UserProfile user);
	public void deleteUser(UserProfile user);
}
