package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Catalogo;

@Repository
public class CatalogRepositoryImpl implements CatalogRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from Catalogo order by title";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}

	public void insertCatalog(Catalogo catalog) {
		entityManagerUtilities.addDB(catalog);	
	}

	public void updateCatalog(Catalogo catalog) {
		entityManagerUtilities.updateDB(Catalogo.class, catalog, catalog.getCatalogId());
	}
	
	public void deleteCatalog(Catalogo catalog) {
		entityManagerUtilities.deleteDB(Catalogo.class, catalog.getCatalogId());
	}

}
