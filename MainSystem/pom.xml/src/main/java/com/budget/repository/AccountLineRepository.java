package com.budget.repository;

import java.util.List;

import com.entities.AccountLine;
import com.transaction.EntityManagerUtilities;

public interface AccountLineRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public List<Object> retrieveAccountLineById(String icHeader);
	public void insertAccount(AccountLine account);
	public void updateAccount(AccountLine account);
	public void deleteAccount(AccountLine account);
}
