package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.entities.Shipping;

@Repository
public class ShippingRepositoryImpl implements ShippingRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from AccountHeader order by accountDate desc";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public List<Object> retrieveArriveItems(String status) {
		String query = "from Shipping s where (s.catalogId||s.productId||s.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status='" + status + "') order by s.catalogId,s.productId,s.trackingNumber";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveDeliveredItems(String status) {
		String query = "from Shipping s where (s.catalogId||s.productId||s.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status='" + status + "') and (s.postalService like '%Olva%' or s.postalService like '%Cruz%') order by s.catalogId,s.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchShippingTracking(String trackId) {
		String query = "from Shipping where trackingNumber = '" + trackId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}

	public void insertShipping(Shipping shipping) {
		entityManagerUtilities.addDB(shipping);	
	}

	public void updateShipping(Shipping shipping) {
		entityManagerUtilities.updateDB(Shipping.class, shipping, shipping.getShippedId());
	}
	
	public void deleteShipping(Shipping shipping) {
		entityManagerUtilities.deleteDB(Shipping.class, shipping.getShippedId());
	}

}
