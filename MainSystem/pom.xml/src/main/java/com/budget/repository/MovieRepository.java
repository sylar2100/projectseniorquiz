package com.budget.repository;

import java.util.List;

import com.entities.Movies;
import com.transaction.EntityManagerUtilities;

public interface MovieRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public void insertMovie(Movies movie);
	public void updateMovie(Movies movie);
	public void deleteMovie(Movies movie);
}
