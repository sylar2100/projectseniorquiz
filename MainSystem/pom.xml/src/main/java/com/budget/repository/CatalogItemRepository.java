package com.budget.repository;

import java.util.List;

import com.entities.CatalogoItem;
import com.transaction.EntityManagerUtilities;

public interface CatalogItemRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public List<Object> findAllTemplate();
	public Object retrieveByKey(String catId, String prodId, String itemNumber);
	public List<Object> retrieveShippingFromForeign(String status);
	public List<Object> retrieveReadyforDelivery();
	public void insertCatalogoItem(CatalogoItem catalog);
	public void updateCatalogoItem(CatalogoItem catalog);
	public void deleteCatalogoItem(CatalogoItem catalog);
}
