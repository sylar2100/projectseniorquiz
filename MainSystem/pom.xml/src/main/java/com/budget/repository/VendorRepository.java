package com.budget.repository;

import java.util.List;

import com.entities.Vendor;
import com.transaction.EntityManagerUtilities;

public interface VendorRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public Object retrieveById(String vendorId);
	public void insertVendor(Vendor vendor);
	public void updateVendor(Vendor vendor);
	public void deleteVendor(Vendor vendor);
}
