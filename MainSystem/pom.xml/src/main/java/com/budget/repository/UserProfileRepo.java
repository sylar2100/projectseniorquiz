package com.budget.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entities.UserProfile;

@Repository
@Transactional
public interface UserProfileRepo extends JpaRepository<UserProfile, String> {
    @Query("SELECT * FROM UserProfile up  WHERE up.userId=(:userId) AND up.userPassword= (:userPassword)")
    UserProfile findByUserIdAndPass(@Param("userId") String userId, @Param("userPassword") String userPassword);
}
