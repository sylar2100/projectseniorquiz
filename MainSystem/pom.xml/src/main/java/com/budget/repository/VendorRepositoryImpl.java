package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Vendor;

@Repository
public class VendorRepositoryImpl implements VendorRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from Vendor order by vendorId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public Object retrieveById(String vendorId) {
		// TODO Auto-generated method stub
		String query = "from Vendor where vendorId = '" + vendorId + "' order by vendorId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		Object result = null;
		if(queryResult != null && queryResult.size() > 0) {
			result = queryResult.get(0);
		}
	
		return result;
	}

	@Override
	public void insertVendor(Vendor vendor) {
		entityManagerUtilities.addDB(vendor);	
	}

	@Override
	public void updateVendor(Vendor vendor) {
		entityManagerUtilities.updateDB(Vendor.class, vendor, vendor.getVendorId());	
	}

	@Override
	public void deleteVendor(Vendor vendor) {
		entityManagerUtilities.deleteDB(Vendor.class, vendor.getVendorId());
	}

}
