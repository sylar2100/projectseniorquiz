package com.budget.repository;

import java.util.List;

import com.entities.Budget;
import com.transaction.EntityManagerUtilities;

public interface BudgetRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public Object retrieveById(String budgetId);
	public void insertBudget(Budget budget);
	public void updateBudget(Budget budget);
	public void deleteBudget(Budget budget);
}
