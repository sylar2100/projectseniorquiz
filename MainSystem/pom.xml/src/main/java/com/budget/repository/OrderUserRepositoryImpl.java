package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Catalogo;
import com.entities.OrderUser;

@Repository
public class OrderUserRepositoryImpl implements OrderUserRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from OrderUser order by userId, poNumber";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public Object retrieveByKey(String catId, String prodId, String itemNumber) {
		Object result = null;
		String query = "from OrderUser where catalogId = '" + catId + "' and productId = '" + prodId + "' and itemNumber = '" + itemNumber + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
	
	public List<Object> preOrdenPartiallyPaid() {
		String query = "from OrderUser o where (o.catalogId||o.productId||o.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status ='04') and o.status='10' order by o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		return queryResult;
	}
	public List<Object> madeOrderRetrieve() {
		String query = "from OrderUser o where (o.catalogId||o.productId||o.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status ='03') and o.status='10' order by o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		return queryResult;
	}
	
	public List<Object> preOrdenInStock() {
		String query = "from OrderUser o where (o.catalogId||o.productId||o.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status > '03' and c.status <= '05') and o.status='10' order by o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		return queryResult;
	}
	
	public List<Object> ordenInStock() {
		String query = "from OrderUser o where (o.catalogId||o.productId||o.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status in ('07','13')) and o.status in ('10','11') order by o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		return queryResult;
	}
	
	public List<Object> retrieveDoubt() {
		String query = "select o.userId,c.description,c.version,c.cost,c.salesCost,o.paidAmount,o.paidSend,(c.salesCost + o.paidSend - o.paidAmount) as debt, (c.cost / 3.35) as realCost from CatalogoItem c,OrderUser o  where o.accountId = c.accountId and c.status in ('05','06','07','13') and o.status <> '15' order by o.userId,o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}

	public List<Object> orderUserDeliveryByPendingPay() {
		String query = "select o.userId,c.description,c.version,c.cost,c.salesCost,o.paidAmount,o.paidSend,(c.salesCost + o.paidSend - o.paidAmount) as debt, (c.cost / 3.35) as realCost from CatalogoItem c,OrderUser o  where o.accountId = c.accountId and c.status = '08' and o.status='11' order by o.userId,o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> doubtOrderUserStock() {
		String query = "select o.userId,c.description,c.version,c.cost,c.salesCost,o.paidAmount,o.paidSend,(c.salesCost + o.paidSend - o.paidAmount) as debt, (c.cost / 3.35) as realCost from CatalogoItem c,OrderUser o  where o.accountId = c.accountId and (c.status = '07' or c.status = '13') and o.status='11' order by o.userId,o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchOrderByUser(String userId) {
		String query = "from OrderUser where userId = '" + userId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveReadyforDelivery() {
		String query = "from OrderUser o where (o.catalogId||o.productId||o.itemNumber) in (select (c.id.catalogId||c.id.productId||c.id.itemNumber) from CatalogoItem c where c.status ='13') and o.status='12' order by o.catalogId,o.productId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}

	public void insertOrderUser(OrderUser order) {
		entityManagerUtilities.addDB(order);	
	}

	public void updateOrderUser(OrderUser order) {
		entityManagerUtilities.updateDB(OrderUser.class, order, order.getIcPoHeader());
	}
	
	public void deleteOrderUser(OrderUser order) {
		entityManagerUtilities.deleteDB(OrderUser.class, order.getIcPoHeader());
	}
}
