package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.AccountHeader;
import com.entities.AccountLine;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from AccountHeader order by accountDate desc";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public Object retrieveAccountById(String icHeader) {
		// TODO Auto-generated method stub
		String query = "from AccountHeader where icHeader = '" + icHeader + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		Object result = null;
		if(queryResult != null && queryResult.size() > 0) {
			result = queryResult.get(0);
		}
	
		return result;
	}

	public void insertAccount(AccountHeader account) {
		entityManagerUtilities.addDB(account);	
	}

	public void updateAccount(AccountHeader account) {
		entityManagerUtilities.updateDB(AccountHeader.class, account, account.getIcHeader());
	}
	
	public void deleteAccount(AccountHeader account) {
		entityManagerUtilities.deleteDB(AccountHeader.class, account.getIcHeader());
	}
	
	public void insertAccountLine(AccountLine account) {
		entityManagerUtilities.addDB(account);	
	}

	public void updateAccountLine(AccountLine account) {
		entityManagerUtilities.updateDB(AccountLine.class, account, account.getIcLine());
	}
	
	public void deleteAccountLine(AccountLine account) {
		entityManagerUtilities.deleteDB(AccountLine.class, account.getIcLine());
	}

}
