package com.budget.repository;

import java.util.List;

import com.entities.Shipping;
import com.transaction.EntityManagerUtilities;

public interface ShippingRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public List<Object> searchShippingTracking(String trackId);
	public List<Object> retrieveArriveItems(String status);
	public List<Object> retrieveDeliveredItems(String status);
	public void insertShipping(Shipping shipping);
	public void updateShipping(Shipping shipping);
	public void deleteShipping(Shipping shipping);
}
