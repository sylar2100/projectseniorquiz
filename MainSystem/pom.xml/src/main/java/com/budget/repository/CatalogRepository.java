package com.budget.repository;

import java.util.List;

import com.entities.Catalogo;
import com.transaction.EntityManagerUtilities;

public interface CatalogRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> findAll();
	public void insertCatalog(Catalogo catalog);
	public void updateCatalog(Catalogo catalog);
	public void deleteCatalog(Catalogo catalog);
}
