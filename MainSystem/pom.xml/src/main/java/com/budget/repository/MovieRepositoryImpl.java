package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.Movies;

@Repository
public class MovieRepositoryImpl implements MovieRepository {

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from Movies order by originalTitle";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}

	public void insertMovie(Movies movie) {
		entityManagerUtilities.addDB(movie);	
	}

	public void updateMovie(Movies movie) {
		entityManagerUtilities.updateDB(Movies.class, movie, movie.getMovieId());
	}
	
	public void deleteMovie(Movies movie) {
		entityManagerUtilities.deleteDB(Movies.class, movie.getMovieId());
	}

}
