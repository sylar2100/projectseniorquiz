package com.budget.repository;

import java.util.List;

import com.entities.Capital;
import com.transaction.EntityManagerUtilities;

public interface CapitalRepository {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public Object findAll();
	public List<Object> retrieveByClassB();
	public void insertCapital(Capital capital);
	public void updateCapital(Capital capital);
	public void deleteCapital(Capital capital);
}
