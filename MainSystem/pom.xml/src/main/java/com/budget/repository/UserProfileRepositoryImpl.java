package com.budget.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entities.UserProfile;

@Repository
public class UserProfileRepositoryImpl implements UserProfileRepository {

	public Object findByUserIdAndPass(String userId, String userPassword) {
		String query = "from UserProfile where userId = '" + userId + "' and userPassword = '" + userPassword + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		Object obj = null;
		if(queryResult.size() > 0) {
			obj = queryResult.get(0);
		}
		return obj;
	}

	public List<Object> findAll() {
		// TODO Auto-generated method stub
		String query = "from UserProfile where status='01' order by userId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public List<Object> sorteo() {
		// TODO Auto-generated method stub
		String query = "from Sorteo";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
	
		return queryResult;
	}
	
	public void insertUser(UserProfile user) {
		entityManagerUtilities.addDB(user);	
	}

	public void updateUser(UserProfile user) {
		entityManagerUtilities.updateDB(UserProfile.class, user, user.getUserId());
	}
	
	public void deleteUser(UserProfile user) {
		entityManagerUtilities.deleteDB(UserProfile.class, user.getUserId());
	}

}
