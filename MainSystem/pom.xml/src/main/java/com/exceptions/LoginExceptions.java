package com.exceptions;

public class LoginExceptions extends Exception {
	private int codigoError;
    
    public LoginExceptions(int codigoError){
        super();
        this.codigoError=codigoError;
    }
     
    @Override
    public String getMessage(){
         
        String message="";
         
        switch(codigoError){
            case 1:
            	message="The user and password aren't correct.";
                break;
        }
         
        return message;
         
    }
}
