package com.main.vendor;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchVendor {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> executeClass(String storeName, String vendorEmail) {
		return this.searchAction(storeName,vendorEmail);		
	}
	
	public List<Object> searchAction(String storeName, String vendorEmail) {
		String query = "from Vendor where storeName like '%" + storeName + "%' and vendorEmail = '" + vendorEmail + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> allVendors() {
		String query = "from Vendor";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
