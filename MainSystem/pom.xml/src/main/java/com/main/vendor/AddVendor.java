package com.main.vendor;

import java.util.Scanner;

import com.entities.Vendor;
import com.transaction.EntityManagerUtilities;

public class AddVendor {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Vendor vendor) {
		Vendor v = new Vendor();
		Scanner sc = new Scanner(System.in);
		if(vendor != null) {
			v.setStoreName(vendor.getStoreName());
			v.setVendorEmail(vendor.getVendorEmail());
		} else {
			System.out.println("Store Name:");
			v.setStoreName(sc.nextLine());
			System.out.println("Website:");
			v.setVendorEmail(sc.nextLine());
		}
		System.out.println("Country:");
		v.setCountry(sc.nextLine());
		v.setVendorId(v.getStoreName().toUpperCase());

		String result = this.insertVendor(v);
		System.out.println(result);
		
	}
	
	public String insertVendor(Vendor v) {
		String message = "";
		entityManagerUtilities.addDB(v);
		message = "Add Successfully with Id " + v.getVendorId();		
		return message;		
	}
}
