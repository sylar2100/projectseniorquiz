package com.main.vendor;

import com.entities.Vendor;
import com.transaction.EntityManagerUtilities;

public class DeleteVendor {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Vendor vendor) {
		String result = this.deleteVendor(vendor);
		System.out.println(result);
		
	}
	
	public String deleteVendor(Vendor vendor) {
		String message = "";
		entityManagerUtilities.deleteDB(Vendor.class, vendor.getVendorId());
		message += "Remove Suceesfully " + vendor.getVendorId() + "; ";
		return message;		
	}
}
