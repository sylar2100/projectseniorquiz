package com.main.vendor;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import com.main.currency.SearchCurrency;
import com.transaction.EntityManagerUtilities;
import com.entities.Capital;
import com.entities.Company;
import com.entities.Currency;
import com.entities.Vendor;

public class UpdateVendor {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Vendor v) {
		Scanner sc = new Scanner(System.in);
		String cad = "";
		System.out.println("Store Name:" + v.getStoreName());
		cad = sc.nextLine();
		v.setStoreName(cad.equals("-") ? v.getStoreName() : cad);
		System.out.println("Website:" + v.getVendorEmail());
		cad = sc.nextLine();
		v.setVendorEmail(cad.equals("-") ? v.getVendorEmail() : cad);
		System.out.println("Country:" + v.getCountry());
		cad = sc.nextLine();
		v.setCountry(cad.equals("-") ? v.getCountry() : cad);
		
		String result = this.updateVendor(v);
		System.out.println(result);
		
	}
	
	public String updateVendor(Vendor v) {
		String message = "";
		entityManagerUtilities.updateDB(Vendor.class, v, v.getVendorId());
		message += "Update Suceesfully " + v.getVendorId() + "; ";
		return message;		
	}
}
