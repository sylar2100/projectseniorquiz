package com.main.orderuser;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchOrderUser {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public static SearchOrderUser orderUser = null;

	public static SearchOrderUser getInstance(){
		if(orderUser == null){
			orderUser = new SearchOrderUser();
		}
		return orderUser;
	}
	public List<Object> executeClass(String orderId) {
		return this.searchAction(orderId);		
	}
	
	public List<Object> searchAction(String orderId) {
		String query = "from OrderUser where icPoHeader = '" + orderId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object searchOrderByAccountId(String accountId) {
		Object result = null;
		String query = "from OrderUser where accountId = '" + accountId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
	
	public List<Object> searchOrderByUser(String userId) {
		String query = "from OrderUser where userId = '" + userId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchOrderByUserIncomplete(String userId) {
		String query = "from OrderUser where userId = '" + userId + "' AND (status <> '15' and status <> '14')";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchOrderByUserPaid(String userId) {
		String query = "from OrderUser where userId = '" + userId + "' AND status = '12'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object retrieveOrderByCatalogIdAndProductIdAndItemNumber(String catId, String prodId, String itemNumber) {
		Object result = null;
		String query = "from OrderUser where catalogId = '" + catId + "' and productId = '" + prodId + "' and itemNumber = '" + itemNumber + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
	
}
