package com.main.orderuser;

import java.math.BigDecimal;
import java.util.EnumSet;
import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Service;

import com.transaction.EntityManagerUtilities;
import com.common.DocumentStatus;
import com.entities.OrderUser;
import com.entities.UserProfile;
import com.main.userProfile.SearchPerson;

@Service
public class OrderUserUpdate {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(OrderUser orderUser) {
		this.executeClass(orderUser, BigDecimal.ZERO);
	}
	public void executeClass(OrderUser orderUser, BigDecimal amountToPay) {
		Scanner sc = new Scanner(System.in);
		Scanner scn = new Scanner(System.in);
		String cad = "";
		SearchPerson searchPerson = new SearchPerson();
		List<Object> queryResult = searchPerson.retrieveAllRequesters();
		if(queryResult.size() > 0) {
			for(int up = 0; up < queryResult.size(); up++) {
				UserProfile userProfile = (UserProfile) queryResult.get(up);
				System.out.println(userProfile.getUserId());
			}
		}
		System.out.println("User Id:" + orderUser.getUserId());
		cad = sc.nextLine();
		orderUser.setUserId(cad.equals("-") ? orderUser.getUserId() : cad);
		System.out.print("Paid Amount:" + orderUser.getPaidAmount().toString());
		BigDecimal num = scn.nextBigDecimal();
		num = num.add(orderUser.getPaidAmount());
		orderUser.setPaidAmount(num);
		System.out.print("Paid Send:" + orderUser.getPaidSend().toString());
		num = scn.nextBigDecimal();
		orderUser.setPaidSend(num.compareTo(BigDecimal.ZERO) == 0 ? orderUser.getPaidSend() : num);
		System.out.print("Account Id:" + orderUser.getAccountId().toString());
		num = scn.nextBigDecimal();
		orderUser.setAccountId(num.compareTo(BigDecimal.ZERO) == 0 ? orderUser.getAccountId() : num);
		EnumSet<DocumentStatus> set = EnumSet.range(DocumentStatus.Separated, DocumentStatus.Closed);
		for(DocumentStatus ds : set){
            System.out.println( ds.name() + ":" + ds.getKey());
        }
		System.out.println("Status:" + orderUser.getStatus());
		if(amountToPay.compareTo(BigDecimal.ZERO) != 0) {
			amountToPay = amountToPay.add(orderUser.getPaidSend());
			amountToPay = amountToPay.subtract(orderUser.getPaidAmount());
			if(amountToPay.compareTo(BigDecimal.ZERO) == 0) {
				orderUser.setStatus("12");
			} 
			System.out.println("NewStatus:" + orderUser.getStatus());
		} else {
			cad = sc.nextLine();
			orderUser.setStatus(cad.equals("-") ? orderUser.getStatus() : cad);
		}
		System.out.println("You want to update PoNumber:");
		if(sc.nextLine().equalsIgnoreCase("Y")) {
			queryResult = SearchOrderUser.getInstance().searchOrderByUser(orderUser.getUserId());
			if(queryResult.size() > 0) {
				BigDecimal in = new BigDecimal(queryResult.size()).add(BigDecimal.ONE);
				orderUser.setPoNumber(in.toString());
			} else {
				orderUser.setPoNumber("1");
			}
		}
	
		String result = this.updateOrderUser(orderUser);
		System.out.println(result);
		
	}
	
	public String updateOrderUser(OrderUser orderUser) {
		String message = "";
		entityManagerUtilities.updateDB(OrderUser.class, orderUser, orderUser.getIcPoHeader());
		message += "Update Suceesfully " + orderUser.getIcPoHeader().toString() + ";";
		return message;		
	}
}
