package com.main.orderuser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.OrderUserRepositoryImpl;
import com.entities.OrderUser;

@Service
public class DeleteOrderUser {
	@Autowired
	private OrderUserRepositoryImpl orderUserRepositoryImpl;
	public void executeClass(OrderUser orderUser) {
		if(orderUser != null) {
			orderUserRepositoryImpl.deleteOrderUser(orderUser);
		} 	
	}
}

