package com.main.orderuser;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Scanner;

import com.common.DocumentStatus;
import com.common.HiltonUtility;
import com.common.UniqueKeyGenerator;
import com.entities.Catalogo;
import com.entities.CatalogoItem;
import com.entities.OrderUser;
import com.entities.Shipping;
import com.entities.UserProfile;
import com.main.catalog.SearchCatalog;
import com.main.catalogItem.SearchCatalogItem;
import com.main.userProfile.SearchPerson;
import com.transaction.EntityManagerUtilities;

public class OrderUserAdd {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass() {
		 executeClass(null, BigDecimal.ZERO);
	}
	
	public void executeClass(OrderUser orderUser, BigDecimal amountToPay) {
		Scanner sc = new Scanner(System.in);
		Scanner scn = new Scanner(System.in);
		if(orderUser == null) {
			orderUser = new OrderUser();
		}
		UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
		orderUser.setIcPoHeader(new BigDecimal(ukg.getUniqueKey()));
		SearchCatalog searchCatalog = new SearchCatalog();
		SearchCatalogItem searchCatalogItem = new SearchCatalogItem();
		SearchPerson searchPerson = new SearchPerson();
		List<Object> queryResult = new ArrayList<Object>();
		if(HiltonUtility.isEmpty(orderUser.getUserId())) {
			queryResult = searchPerson.retrieveAllRequesters();
			if(queryResult.size() > 0) {
				for(int up = 0; up < queryResult.size(); up++) {
					UserProfile userProfile = (UserProfile) queryResult.get(up);
					System.out.println(userProfile.getUserId());
				}
			}
			System.out.println("User Id:");
			orderUser.setUserId(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(orderUser.getBuyerId())) {
			queryResult = searchPerson.retrieveAllBuyers();
			if(queryResult.size() > 0) {
				for(int up = 0; up < queryResult.size(); up++) {
					UserProfile userProfile = (UserProfile) queryResult.get(up);
					System.out.println(userProfile.getUserId());
				}
			}
			System.out.println("Buyer Id:");
			orderUser.setBuyerId(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(orderUser.getPaidAmount())) {
			System.out.print("Paid Amount:");
			orderUser.setPaidAmount(scn.nextBigDecimal());
		} 
		if(HiltonUtility.isEmpty(orderUser.getPaidSend())) {
			System.out.print("Paid Send:");
			orderUser.setPaidSend(scn.nextBigDecimal());
		}
		if(HiltonUtility.isEmpty(orderUser.getAccountId())) {
			orderUser.setAccountId(BigDecimal.ZERO);
		}
		if(HiltonUtility.isEmpty(orderUser.getStatus())) {
			EnumSet<DocumentStatus> set = EnumSet.range(DocumentStatus.Separated, DocumentStatus.Closed);
			for(DocumentStatus ds : set){
	            System.out.println( ds.name() + ":" + ds.getKey());
	        }
			System.out.println("Status:" + orderUser.getStatus());
			if(amountToPay.compareTo(BigDecimal.ZERO) != 0) {
				amountToPay = amountToPay.add(orderUser.getPaidSend());
				amountToPay = amountToPay.subtract(orderUser.getPaidAmount());
				if(amountToPay.compareTo(BigDecimal.ZERO) == 0) {
					orderUser.setStatus("12");
				}
			} else {
				orderUser.setStatus(sc.nextLine());
			}
		}
		
		orderUser.setOrderDate(new Date());
		if(orderUser.getStatus().equalsIgnoreCase("12")) {
			orderUser.setPaidDate(new Date());
		}
		if(HiltonUtility.isEmpty(orderUser.getCatalogId())) {
			queryResult = searchCatalog.allCatalogs();
			if(queryResult.size() > 0) {
				for(int c = 0; c < queryResult.size(); c++) {
					Catalogo catalog = (Catalogo) queryResult.get(c);
					System.out.println(catalog.getCatalogId());
				}
			}
			System.out.println("Catalog Id:");
			orderUser.setCatalogId(sc.nextLine().toUpperCase());
		}
		
		if(HiltonUtility.isEmpty(orderUser.getProductId())) {
			queryResult = searchCatalogItem.retrieveDistinctByCatalogId(orderUser.getCatalogId());
			if(queryResult.size() > 0) {
				for(int ci = 0; ci < queryResult.size(); ci++) {
					String productId = (String) queryResult.get(ci);
					System.out.println(productId);
				}
			}
			System.out.println("Product Id:");
			orderUser.setProductId(sc.nextLine().toUpperCase());
		}
		
		if(HiltonUtility.isEmpty(orderUser.getItemNumber())) {
			queryResult = searchCatalogItem.retrieveByCatalogIdAndProductId(orderUser.getCatalogId(), orderUser.getProductId());
			if(queryResult.size() > 0) {
				for(int ci = 0; ci < queryResult.size(); ci++) {
					CatalogoItem catalogItem = (CatalogoItem) queryResult.get(ci);
					System.out.println(catalogItem.getId().getItemNumber());
				}
			}
			System.out.println("Item Number:");
			orderUser.setItemNumber(sc.nextLine());
		}
		
		if(HiltonUtility.isEmpty(orderUser.getPoNumber())) {
			queryResult = SearchOrderUser.getInstance().searchOrderByUser(orderUser.getUserId());
			if(queryResult.size() > 0) {
				BigDecimal in = new BigDecimal(queryResult.size()).add(BigDecimal.ONE);
				orderUser.setPoNumber(in.toString());
			} else {
				orderUser.setPoNumber("1");
			}
		}
		
		String result = this.insertOrderUser(orderUser);
		System.out.println(result);
		
	}
	
	public String insertOrderUser(OrderUser orderUser) {
		String message = "";
		entityManagerUtilities.addDB(orderUser);
		message = "Add Successfully with Id " + orderUser.getIcPoHeader().toString() + ";";		
		return message;		
	}
}
