package com.main.orderuser;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogRepositoryImpl;
import com.budget.repository.OrderUserRepositoryImpl;
import com.common.DocumentStatus;
import com.common.HiltonUtility;
import com.common.UniqueKeyGenerator;
import com.entities.Catalogo;
import com.entities.CatalogoItem;
import com.entities.OrderUser;
import com.entities.Shipping;
import com.entities.UserProfile;
import com.main.catalog.SearchCatalog;
import com.main.catalogItem.SearchCatalogItem;
import com.main.userProfile.SearchPerson;
import com.transaction.EntityManagerUtilities;

@Service
public class AddOrderUser {
	@Autowired
	private OrderUserRepositoryImpl orderUserRepositoryImpl;
	public void executeClass(OrderUser orderUser) {
		if(orderUser != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			orderUser.setIcPoHeader(new BigDecimal(ukg.getUniqueKey()));
			orderUser.setOrderDate(new Date());
			if(orderUser.getStatus().equalsIgnoreCase("12")) {
				orderUser.setPaidDate(new Date());
			}
			if(HiltonUtility.isEmpty(orderUser.getPoNumber())) {
				List<Object> queryResult = orderUserRepositoryImpl.searchOrderByUser(orderUser.getUserId());
				if(queryResult.size() > 0) {
					BigDecimal in = new BigDecimal(queryResult.size()).add(BigDecimal.ONE);
					orderUser.setPoNumber(in.toString());
				} else {
					orderUser.setPoNumber("1");
				}
			}
			orderUserRepositoryImpl.insertOrderUser(orderUser);
		}
	}
}
