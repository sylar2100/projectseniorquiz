package com.main.orderuser;

import com.entities.OrderUser;
import com.transaction.EntityManagerUtilities;

public class OrderUserDelete {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(OrderUser orderUser) {
		String result = this.deleteOrderUser(orderUser);
		System.out.println(result);
		
	}
	
	public String deleteOrderUser(OrderUser orderUser) {
		String message = "";
		entityManagerUtilities.deleteDB(OrderUser.class, orderUser.getIcPoHeader());
		message += "Remove Suceesfully " + orderUser.getIcPoHeader().toString() + ";";
		return message;		
	}
}
