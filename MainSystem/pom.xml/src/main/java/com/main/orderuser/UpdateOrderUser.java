package com.main.orderuser;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.OrderUserRepositoryImpl;
import com.common.HiltonUtility;
import com.entities.OrderUser;

@Service
public class UpdateOrderUser {
	@Autowired
	private OrderUserRepositoryImpl orderUserRepositoryImpl;
	public void executeClass(OrderUser orderUser) {
		if(orderUser != null) {
			if(HiltonUtility.isEmpty(orderUser.getPoNumber())) {
				List<Object> queryResult = orderUserRepositoryImpl.searchOrderByUser(orderUser.getUserId());
				if(queryResult.size() > 0) {
					BigDecimal in = new BigDecimal(queryResult.size()).add(BigDecimal.ONE);
					orderUser.setPoNumber(in.toString());
				} else {
					orderUser.setPoNumber("1");
				}
			}
			orderUserRepositoryImpl.updateOrderUser(orderUser);
		} 	
	}
}
