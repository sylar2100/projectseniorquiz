package com.main.shipping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.ShippingRepositoryImpl;
import com.common.HiltonUtility;
import com.entities.Shipping;

@Service
public class UpdateShipping {
	@Autowired
	private ShippingRepositoryImpl shippingRepositoryImpl;
	public void executeClass(Shipping shipping) {
		if(shipping != null) {
			if(shipping.getSendDate() != null) {
				shipping.setSendDate(HiltonUtility.sumarDiasAFecha(shipping.getSendDate(), 1));
			}
			shippingRepositoryImpl.updateShipping(shipping);
		} 	
	}
}
