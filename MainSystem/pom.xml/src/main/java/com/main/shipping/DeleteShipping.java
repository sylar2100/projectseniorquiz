package com.main.shipping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.ShippingRepositoryImpl;
import com.entities.Shipping;

@Service
public class DeleteShipping {
	@Autowired
	private ShippingRepositoryImpl shippingRepositoryImpl;
	public void executeClass(Shipping shipping) {
		if(shipping != null) {
			shippingRepositoryImpl.deleteShipping(shipping);
		} 	
	}
}
