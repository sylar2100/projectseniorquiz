package com.main.shipping;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.ShippingRepositoryImpl;
import com.common.HiltonUtility;
import com.common.UniqueKeyGenerator;
import com.entities.Catalogo;
import com.entities.CatalogoItem;
import com.entities.Shipping;
import com.main.catalog.SearchCatalog;
import com.main.catalogItem.SearchCatalogItem;
import com.transaction.EntityManagerUtilities;

@Service
public class AddShipping {
	@Autowired
	private ShippingRepositoryImpl shippingRepositoryImpl;
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeProcess(Shipping shipping) {
		if(shipping != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			shipping.setShippedId(new BigDecimal(ukg.getUniqueKey()));
			if(shipping.getSendDate() != null) {
				shipping.setSendDate(HiltonUtility.sumarDiasAFecha(shipping.getSendDate(), 1));
			}
			shippingRepositoryImpl.insertShipping(shipping);
		} 
	}
	public void executeClass() {
		this.executeClass(null);
	}
	
	public void executeClass(Shipping s) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Scanner sc = new Scanner(System.in);
		if(s == null) {
			s = new Shipping();
		}
		UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
		s.setShippedId(new BigDecimal(ukg.getUniqueKey()));
		if(HiltonUtility.isEmpty(s.getPostalService())) {
			System.out.println("Postal Service:");
			s.setPostalService(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(s.getTrackingNumber())) {
			System.out.print("Tracking Number:");
			s.setTrackingNumber(sc.nextLine());
		}
		System.out.println("Send Date (dd/MM/yyyy):");
		try {
			s.setSendDate(formatter.parse(sc.nextLine()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SearchCatalog searchCatalog = new SearchCatalog();
		SearchCatalogItem searchCatalogItem = new SearchCatalogItem();
		List<Object> queryResult = searchCatalog.allCatalogs();
		if(HiltonUtility.isEmpty(s.getCatalogId())) {
			if(queryResult.size() > 0) {
				for(int c = 0; c < queryResult.size(); c++) {
					Catalogo catalog = (Catalogo) queryResult.get(c);
					System.out.println(catalog.getCatalogId());
				}
			}
			System.out.println("Catalog Id:");
			s.setCatalogId(sc.nextLine().toUpperCase());
		}
		
		if(HiltonUtility.isEmpty(s.getProductId())) {
			queryResult = searchCatalogItem.retrieveByCatalogId(s.getCatalogId());
			if(queryResult.size() > 0) {
				for(int ci = 0; ci < queryResult.size(); ci++) {
					CatalogoItem catalogItem = (CatalogoItem) queryResult.get(ci);
					System.out.println(catalogItem.getId().getProductId());
				}
			}
			System.out.println("Product Id:");
			s.setProductId(sc.nextLine().toUpperCase());
		}
		
		if(HiltonUtility.isEmpty(s.getItemNumber())) {
			queryResult = searchCatalogItem.retrieveByCatalogIdAndProductId(s.getCatalogId(), s.getProductId());
			if(queryResult.size() > 0) {
				for(int ci = 0; ci < queryResult.size(); ci++) {
					CatalogoItem catalogItem = (CatalogoItem) queryResult.get(ci);
					System.out.println(catalogItem.getId().getItemNumber());
				}
			}
			System.out.println("Item Number:");
			s.setItemNumber(sc.nextLine());
		}
		String result = this.insertShipping(s);
		System.out.println(result);
		
	}
	
	public String insertShipping(Shipping shipping) {
		String message = "";
		entityManagerUtilities.addDB(shipping);
		message = "Add Successfully with Id " + shipping.getShippedId() + ";";		
		return message;		
	}
}
