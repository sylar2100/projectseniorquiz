package com.main.shipping;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchShipping {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> executeClass(String shipId) {
		return this.searchAction(shipId);		
	}
	
	public List<Object> searchAction(String shipId) {
		String query = "from Shipping where shippedId like '%" + shipId + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchShippingyTracking(String trackId) {
		String query = "from Shipping where trackingNumber like '%" + trackId + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
