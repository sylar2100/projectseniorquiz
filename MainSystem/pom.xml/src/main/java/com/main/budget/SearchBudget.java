package com.main.budget;

import java.math.BigDecimal;
import java.util.List;

import com.main.currency.SearchCurrency;
import com.transaction.EntityManagerUtilities;

public class SearchBudget {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public static SearchBudget searchBudget = null;
	
	public static SearchBudget getInstance(){
		if(searchBudget == null){
			searchBudget = new SearchBudget();
		}
		return searchBudget;
	}
	
	public List<Object> executeClass(String desc) {
		return this.searchAction(desc);		
	}
	
	public List<Object> searchAction(String description) {
		String query = "from Budget where description like '%" + description + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object retrieveById(BigDecimal budgetId) {
		Object budget = null;
		String query = "from Budget where budgetId = '" + budgetId.toString() + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		if(queryResult.size() > 0) {
			budget = queryResult.get(0);
		}
		return budget;
	}
	
	public List<Object> retrieveAll() {
		String query = "from Budget";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
