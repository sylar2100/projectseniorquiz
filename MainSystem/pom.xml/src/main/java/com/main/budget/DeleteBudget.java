package com.main.budget;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.BudgetRepositoryImpl;
import com.entities.Budget;

@Service
public class DeleteBudget {
	@Autowired
	private BudgetRepositoryImpl budgetRepositoryImpl;
	public void executeClass(Budget budget) {
		if(budget != null) {
			budgetRepositoryImpl.deleteBudget(budget);
		} 	
	}
}