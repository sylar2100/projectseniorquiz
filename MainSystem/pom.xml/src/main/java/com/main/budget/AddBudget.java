package com.main.budget;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.BudgetRepositoryImpl;
import com.common.UniqueKeyGenerator;
import com.entities.Budget;

@Service
public class AddBudget {
	@Autowired
	private BudgetRepositoryImpl budgetRepositoryImpl;
	public void executeClass(Budget budget) {
		if(budget != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			budget.setBudgetId(new BigDecimal(ukg.getUniqueKey()));
			budget.setBudgetDate(new Date());
			budget.setBudgetPeriod(new BigDecimal(LocalDate.now().getYear()));
			budgetRepositoryImpl.insertBudget(budget);
		} 
	}
}
