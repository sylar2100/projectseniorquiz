package com.main.budget;

import java.math.BigDecimal;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transaction.EntityManagerUtilities;
import com.budget.repository.BudgetRepositoryImpl;
import com.entities.Budget;


@Service
public class UpdateBudget {
	@Autowired
	private BudgetRepositoryImpl budgetRepositoryImpl;
	public static UpdateBudget updateBudget = null;
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Budget budget) {
		if(budget != null) {
			budgetRepositoryImpl.updateBudget(budget);
		} 	
	}
	
	public static UpdateBudget getInstance(){
		if(updateBudget == null){
			updateBudget = new UpdateBudget();
		}
		return updateBudget;
	}
	
	public void updateBudgetAmount(Budget budget, BigDecimal amount, String add) {
		String message = "";
		Scanner sc = new Scanner(System.in);
		if(budget != null) {
			System.out.println("You want Update the Amount Budget?");
			if(sc.nextLine().equalsIgnoreCase("Y")) {
				if(add.equalsIgnoreCase("Y")) {
					budget.setAmount(budget.getAmount().add(amount));
				} else {
					budget.setAmount(budget.getAmount().subtract(amount));
				}
			}
			if(add.equalsIgnoreCase("Y")) {
				budget.setBalance(budget.getBalance().add(amount));
			} else {
				budget.setBalance(budget.getBalance().subtract(amount));
			}
			entityManagerUtilities.updateDB(Budget.class, budget, budget.getBudgetId());
			message += "Update Suceesfully " + budget.getBudgetId().toString() + "; ";
			System.out.println(message);
		}
	}
	
	public void updateBudgetAmount(Budget budget, BigDecimal amount, String add, boolean replaceAmount) {
		String message = "";
		if(budget != null) {
			if(replaceAmount) {
				if(add.equalsIgnoreCase("Y")) {
					budget.setAmount(budget.getAmount().add(amount));
				} else {
					budget.setAmount(budget.getAmount().subtract(amount));
				}
			}
			if(add.equalsIgnoreCase("Y")) {
				budget.setBalance(budget.getBalance().add(amount));
			} else {
				budget.setBalance(budget.getBalance().subtract(amount));
			}
			entityManagerUtilities.updateDB(Budget.class, budget, budget.getBudgetId());
			message += "Update Suceesfully " + budget.getBudgetId().toString() + "; ";
			System.out.println(message);
		}
	}
}