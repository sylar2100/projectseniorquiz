package com.main.account;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountRepositoryImpl;
import com.entities.AccountHeader;
import com.entities.AccountLine;

@Service
public class UpdateAccount {
	@Autowired
	private AccountRepositoryImpl accountRepositoryImpl;
	public void executeClass(AccountHeader account) {
		if(account != null) {
			accountRepositoryImpl.updateAccount(account);
		} 	
	}
	
	public void executeSubClass(AccountLine account) {
		if(account != null) {
			if(account.getBudgetId() == null) {
				account.setBudgetId(BigDecimal.ZERO);
			}
			if(account.getCapitalId() == null) {
				account.setCapitalId("0");
			}
			accountRepositoryImpl.updateAccountLine(account);
		} 	
	}
}
