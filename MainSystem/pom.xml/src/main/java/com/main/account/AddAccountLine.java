package com.main.account;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.common.DocumentStatus;
import com.common.HiltonUtility;
import com.common.PayPalEmails;
import com.common.UniqueKeyGenerator;
import com.entities.AccountHeader;
import com.entities.AccountLine;
import com.entities.Budget;
import com.entities.Capital;
import com.entities.Catalogo;
import com.entities.Currency;
import com.entities.UserProfile;
import com.main.budget.SearchBudget;
import com.main.currency.SearchCurrency;
import com.transaction.EntityManagerUtilities;

public class AddAccountLine {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass() {
		this.executeClass(null, null);
	}
	
	public void executeClass(AccountHeader accHeader, AccountLine accLine) {
		if(accHeader != null) {
			accLine.setIcHeader(accHeader.getIcHeader());
		} else {
			if(HiltonUtility.isEmpty(accLine.getIcHeader())) {
				accLine.setIcHeader(BigDecimal.ZERO);
			}
		}
		if(accLine == null) {
			accLine = new AccountLine();
		}
		Scanner sc = new Scanner(System.in);
		Scanner scn = new Scanner(System.in);
		UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
		accLine.setIcLine(new BigDecimal(ukg.getUniqueKey()));
		accLine.setAccountDate(new Date());
		if(HiltonUtility.isEmpty(accLine.getIncome())) {
			System.out.println("Income:");
			accLine.setIncome(scn.nextBigDecimal());
		}
		if(HiltonUtility.isEmpty(accLine.getExpenses())) {
			System.out.println("Expenses:");
			accLine.setExpenses(scn.nextBigDecimal());
		}
		if(HiltonUtility.isEmpty(accLine.getPaypalTransaction())) {
			System.out.println("PayPal:");
			accLine.setPaypalTransaction(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(accLine.getPaypalEmail())) {
			for(PayPalEmails pe : PayPalEmails.values()){
	            System.out.println( pe.name() + ":" + pe.getStatusName());
	        }
			System.out.println("PayPal Email:");
			accLine.setPaypalEmail(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(accLine.getCurrencyCode())) {
			List<Object> resultList = SearchCurrency.getInstance().searchAll();
			for(int cur = 0; cur < resultList.size(); cur++) {
				Currency currency = (Currency) resultList.get(cur);
				System.out.println(currency.getCurrencyCode() + ":" + currency.getCurrencyName());
			}
			System.out.println("Currency Code:");
			accLine.setCurrencyCode(sc.nextLine());
		}
		accLine.setCreditCard("N");
		System.out.println("Capital Id:");
		accLine.setCapitalId(sc.nextLine());
		SearchBudget searchBudget = new SearchBudget();
		List<Object> queryResult = searchBudget.retrieveAll();
		if(queryResult.size() > 0) {
			for(int up = 0; up < queryResult.size(); up++) {
				Budget budget = (Budget) queryResult.get(up);
				System.out.println(budget.getBudgetId() + ":" + budget.getDescription());
			}
		}
		System.out.println("Budget Id:");
		accLine.setBudgetId(scn.nextBigDecimal());
		
		String result = this.insertAccountLine(accLine);
		
		System.out.println(result);
	}

	public String insertAccountLine(AccountLine accLine) {
		String message = "";
		entityManagerUtilities.addDB(accLine);
		message = "Add Successfully with Id " + accLine.getIcLine().toString();		
		return message;		
	}
}
