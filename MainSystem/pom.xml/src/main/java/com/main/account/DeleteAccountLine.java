package com.main.account;

import com.entities.AccountLine;
import com.entities.Catalogo;
import com.transaction.EntityManagerUtilities;

public class DeleteAccountLine {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(AccountLine accLine) {
		String result = this.deleteAccountLine(accLine);
		System.out.println(result);
		
	}
	
	public String deleteAccountLine(AccountLine accLine) {
		String message = "";
		entityManagerUtilities.deleteDB(AccountLine.class, accLine.getIcLine());
		message += "Remove Suceesfully " + accLine.getIcLine().toString() + "; ";
		return message;		
	}
}
