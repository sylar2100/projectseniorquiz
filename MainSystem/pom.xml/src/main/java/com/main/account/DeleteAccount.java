package com.main.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountRepositoryImpl;
import com.entities.AccountHeader;
import com.entities.AccountLine;

@Service
public class DeleteAccount {
	@Autowired
	private AccountRepositoryImpl accountRepositoryImpl;
	public void executeClass(AccountHeader account) {
		if(account != null) {
			accountRepositoryImpl.deleteAccount(account);
		} 	
	}
	
	public void executeSubClass(AccountLine account) {
		if(account != null) {
			accountRepositoryImpl.deleteAccountLine(account);
		} 	
	}
}
