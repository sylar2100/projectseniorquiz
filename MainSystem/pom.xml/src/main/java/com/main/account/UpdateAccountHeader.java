package com.main.account;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import com.main.currency.SearchCurrency;
import com.main.userProfile.SearchPerson;
import com.transaction.EntityManagerUtilities;
import com.common.DocumentStatus;
import com.entities.AccountHeader;
import com.entities.Currency;
import com.entities.UserProfile;

public class UpdateAccountHeader {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(AccountHeader accHeader) {
		Scanner sc = new Scanner(System.in);
		String cad = "";
		System.out.println("Description:" + accHeader.getDescription());
		cad = sc.nextLine();
		accHeader.setDescription(cad.equals("-") ? accHeader.getDescription() : cad);
		for(DocumentStatus ds : DocumentStatus.values()){
            System.out.println( ds.name() + ":" + ds.getKey());
        }
		System.out.println("Status:" + accHeader.getStatus());
		cad = sc.nextLine();
		accHeader.setStatus(cad.equals("-") ? accHeader.getStatus() : cad);
		SearchPerson searchPerson = new SearchPerson();
		List<Object> queryResult = searchPerson.retrieveAllRequesters();
		if(queryResult.size() > 0) {
			for(int up = 0; up < queryResult.size(); up++) {
				UserProfile userProfile = (UserProfile) queryResult.get(up);
				System.out.println(userProfile.getUserId());
			}
		}
		System.out.println("User Id:" + accHeader.getUserId());
		cad = sc.nextLine();
		accHeader.setUserId(cad.equals("-") ? accHeader.getUserId() : cad);
		LinkedHashMap<String,String> currMap = new LinkedHashMap<String,String>();
		List<Object> resultList = SearchCurrency.getInstance().searchAll();
		for(int cur = 0; cur < resultList.size(); cur++) {
			Currency currency = (Currency) resultList.get(cur);
			currMap.put(currency.getCurrencyCode(), currency.getCurrencyName());
			System.out.println(currency.getCurrencyCode() + ":" + currency.getCurrencyName());
		}
		System.out.println("Currency Code:" + accHeader.getCurrencyCode());
		cad = sc.nextLine();
		accHeader.setCurrencyCode(cad.equals("-") ? accHeader.getCurrencyCode() : cad);		
		
		String result = this.updateAccountHeader(accHeader);
		System.out.println(result);
		
	}
	
	public String updateAccountHeader(AccountHeader accHeader) {
		String message = "";
		entityManagerUtilities.updateDB(AccountHeader.class, accHeader, accHeader.getIcHeader());
		message += "Update Suceesfully " + accHeader.getIcHeader().toString() + "; ";
		return message;		
	}
}
