package com.main.account;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.common.DocumentStatus;
import com.common.HiltonUtility;
import com.common.UniqueKeyGenerator;
import com.entities.AccountHeader;
import com.entities.Catalogo;
import com.entities.Currency;
import com.entities.UserProfile;
import com.main.currency.SearchCurrency;
import com.main.userProfile.SearchPerson;
import com.transaction.EntityManagerUtilities;

public class AddAccountHeader {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass() {
		this.executeClass(null);
	}
	public void executeClass(AccountHeader accHeader) {
		if(accHeader == null) {
			accHeader = new AccountHeader();
		}
		Scanner sc = new Scanner(System.in);
		UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
		accHeader.setIcHeader(new BigDecimal(ukg.getUniqueKey()));
		if(HiltonUtility.isEmpty(accHeader.getDescription())) {
			System.out.println("Description:");
			accHeader.setDescription(sc.nextLine());
		}
		accHeader.setAccountDate(new Date());
		if(HiltonUtility.isEmpty(accHeader.getStatus())) {
			for(DocumentStatus ds : DocumentStatus.values()){
	            System.out.println( ds.name() + ":" + ds.getKey());
	        }
			System.out.println("Status:");
			accHeader.setStatus(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(accHeader.getUserId())) {
			SearchPerson searchPerson = new SearchPerson();
			List<Object> queryResult = searchPerson.retrieveAllRequesters();
			if(queryResult.size() > 0) {
				for(int up = 0; up < queryResult.size(); up++) {
					UserProfile userProfile = (UserProfile) queryResult.get(up);
					System.out.println(userProfile.getUserId());
				}
			}
			System.out.println("User Id:");
			accHeader.setUserId(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(accHeader.getCurrencyCode())) {
			List<Object> resultList = SearchCurrency.getInstance().searchAll();
			for(int cur = 0; cur < resultList.size(); cur++) {
				Currency currency = (Currency) resultList.get(cur);
				System.out.println(currency.getCurrencyCode() + ":" + currency.getCurrencyName());
			}
			System.out.println("Currency Code:");
			accHeader.setCurrencyCode(sc.nextLine());
		}
		String result = this.insertAccountHeader(accHeader);
		System.out.println(result);
		
	}
	
	public String insertAccountHeader(AccountHeader accHeader) {
		String message = "";
		entityManagerUtilities.addDB(accHeader);
		message = "Add Successfully with Id " + accHeader.getIcHeader().toString();		
		return message;		
	}
}
