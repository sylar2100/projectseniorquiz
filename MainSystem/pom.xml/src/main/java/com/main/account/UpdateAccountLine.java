package com.main.account;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import com.transaction.EntityManagerUtilities;
import com.entities.AccountLine;
import com.entities.UserProfile;
import com.main.userProfile.SearchPerson;

public class UpdateAccountLine {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(AccountLine accLine) {
		Scanner sc = new Scanner(System.in);
		Scanner scn = new Scanner(System.in);
		String cad = "";
		System.out.println("Income:" + accLine.getIncome());
		BigDecimal num = scn.nextBigDecimal();
		accLine.setIncome(num.compareTo(BigDecimal.ZERO) == 0 ? accLine.getIncome() : num);
		System.out.println("Expenses:" + accLine.getExpenses());
		num = scn.nextBigDecimal();
		accLine.setExpenses(num.compareTo(BigDecimal.ZERO) == 0 ? accLine.getExpenses() : num);
		System.out.println("PayPal:" + accLine.getPaypalTransaction());
		cad = sc.nextLine();
		accLine.setPaypalTransaction(cad.equals("-") ? accLine.getPaypalTransaction() : cad);
		SearchPerson searchPerson = new SearchPerson();
		List<Object> queryResult = searchPerson.retrieveAllBuyers();
		if(queryResult.size() > 0) {
			for(int up = 0; up < queryResult.size(); up++) {
				UserProfile userProfile = (UserProfile) queryResult.get(up);
				System.out.println(userProfile.getUserId());
			}
		}
		
		String result = this.updateAccountLine(accLine);
		System.out.println(result);
		
	}
	
	public String updateAccountLine(AccountLine accLine) {
		String message = "";
		entityManagerUtilities.updateDB(AccountLine.class, accLine, accLine.getIcLine());
		message += "Update Suceesfully " + accLine.getIcLine().toString() + "; ";
		return message;		
	}
}
