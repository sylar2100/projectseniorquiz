package com.main.account;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.AccountRepositoryImpl;
import com.common.HiltonUtility;
import com.common.UniqueKeyGenerator;
import com.entities.AccountHeader;
import com.entities.AccountLine;

@Service
public class AddAccount {
	@Autowired
	private AccountRepositoryImpl accountRepositoryImpl;
	public void executeClass(AccountHeader account) {
		if(account != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			if((HiltonUtility.isEmpty(account.getIcHeader()) || account.getIcHeader().compareTo(BigDecimal.ZERO) == 0)) { 
				account.setIcHeader(new BigDecimal(ukg.getUniqueKey()));
			}
			account.setAccountDate(new Date());
			accountRepositoryImpl.insertAccount(account);
		}
	}
	
	public void executeSubClass(AccountLine account) {
		if(account != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			account.setIcLine(new BigDecimal(ukg.getUniqueKey()));
			account.setAccountDate(new Date());
			account.setCreditCard("N");
			if(account.getBudgetId() == null) {
				account.setBudgetId(BigDecimal.ZERO);
			}
			if(account.getCapitalId() == null) {
				account.setCapitalId("0");
			}
			accountRepositoryImpl.insertAccountLine(account);
		}
	}
}
