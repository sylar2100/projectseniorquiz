package com.main.account;

import java.math.BigDecimal;
import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchAccountHeader {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> executeClass(String icHeader) {
		return this.searchAction(icHeader);		
	}
	
	public List<Object> searchAction(String icHeader) {
		String query = "from AccountHeader where icHeader = '" + icHeader + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object searchActionById(BigDecimal icHeader) {
		String query = "from AccountHeader where icHeader = '" + icHeader.toString() + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		Object result = null;
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
	
	public List<Object> searchActionByDesc(String desc) {
		String query = "from AccountHeader where description like '%" + desc + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		return queryResult;		
	}
}
