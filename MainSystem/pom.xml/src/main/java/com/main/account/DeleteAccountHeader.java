package com.main.account;

import com.entities.AccountHeader;
import com.entities.Catalogo;
import com.transaction.EntityManagerUtilities;

public class DeleteAccountHeader {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(AccountHeader accHeader) {
		String result = this.deleteAccountHeader(accHeader);
		System.out.println(result);
		
	}
	
	public String deleteAccountHeader(AccountHeader accHeader) {
		String message = "";
		entityManagerUtilities.deleteDB(AccountHeader.class, accHeader.getIcHeader());
		message += "Remove Suceesfully " + accHeader.getIcHeader().toString() + "; ";
		return message;		
	}
}
