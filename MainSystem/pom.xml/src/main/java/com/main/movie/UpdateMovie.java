package com.main.movie;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.MovieRepositoryImpl;
import com.entities.Movies;

@Service
public class UpdateMovie {
	@Autowired
	private MovieRepositoryImpl movieRepositoryImpl;
	public void executeClass(Movies movie) {
		if(movie != null) {
			movieRepositoryImpl.updateMovie(movie);
		} 	
	}
}
