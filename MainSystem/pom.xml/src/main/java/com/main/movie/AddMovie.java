package com.main.movie;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.MovieRepositoryImpl;
import com.common.UniqueKeyGenerator;
import com.entities.Movies;

@Service
public class AddMovie {
	@Autowired
	private MovieRepositoryImpl movieRepositoryImpl;
	public void executeClass(Movies movie) {
		if(movie != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			movie.setMovieId(new BigDecimal(ukg.getUniqueKey()));
			movie.setImageMovie(movie.getImageMovie());
			movieRepositoryImpl.insertMovie(movie);
		} 
	}
	
}
