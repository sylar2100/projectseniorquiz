package com.main.currency;

import java.math.BigDecimal;
import java.util.Scanner;

import com.entities.Currency;
import com.transaction.EntityManagerUtilities;

public class AddCurrency {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Currency c) {
		Currency currency = new Currency();
		Scanner sc = new Scanner(System.in);
		if(c != null) {
			currency.setCurrencyCode(c.getCurrencyCode());
			currency.setCurrencyName(c.getCurrencyName());
		} else {
			System.out.println("Currency Code:");
			currency.setCurrencyCode(sc.nextLine().toUpperCase());
			System.out.println("Currency Name:");
			currency.setCurrencyName(sc.nextLine().toUpperCase());
		}
		System.out.println("Currency Symbol:");
		c.setCurrencySymbol(sc.nextLine());
		System.out.println("Currency Change:");
		currency.setCurrencyChange(sc.nextBigDecimal());
		
		String result = this.insertCurrency(currency);
		System.out.println(result);
		
	}
	
	public String insertCurrency(Currency currency) {
		String message = "";
		entityManagerUtilities.addDB(currency);
		message = "Add Successfully with Id " + currency.getCurrencyCode();		
		return message;		
	}
}
