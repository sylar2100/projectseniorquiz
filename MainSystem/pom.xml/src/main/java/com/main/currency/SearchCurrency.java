package com.main.currency;

import java.util.List;

import com.common.UniqueKeyGenerator;
import com.entities.Currency;
import com.transaction.EntityManagerUtilities;

public class SearchCurrency {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public static SearchCurrency currency = null;
	public List<Object> executeClass(String currencyCode, String currencyName) {
		return this.searchAction(currencyCode.toUpperCase(), currencyName.toUpperCase());		
	}
	
	public static SearchCurrency getInstance(){
		if(currency == null){
			currency = new SearchCurrency();
		}
		return currency;
	}
	
	public List<Object> searchAction(String currencyCode, String currencyName) {
		String query = "from Currency where (currencyCode = '" + currencyCode + "' or currencyName like '%" + currencyName + "%')";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchAll() {
		String query = "from Currency";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object searchById(String currencyCode) {
		String query = "from Currency where currencyCode = '" + currencyCode + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		Object result = null;
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
}
