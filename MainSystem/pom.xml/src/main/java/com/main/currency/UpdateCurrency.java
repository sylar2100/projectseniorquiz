package com.main.currency;

import java.math.BigDecimal;
import java.util.Scanner;

import com.transaction.EntityManagerUtilities;
import com.entities.Currency;

public class UpdateCurrency {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Currency c) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Currency Name:" + c.getCurrencyName());
		c.setCurrencyName(sc.nextLine().equals("-") ? c.getCurrencyName() : sc.nextLine());
		System.out.println("Currency Symbol:" + c.getCurrencySymbol());
		String cs = sc.nextLine();
		c.setCurrencySymbol(cs.equals("-") ? c.getCurrencySymbol() : cs);
		System.out.println("Currency Change:" + c.getCurrencyChange().toString());
		BigDecimal cc = sc.nextBigDecimal();
		c.setCurrencyChange(cc.compareTo(BigDecimal.ZERO) < 0 ? c.getCurrencyChange() : cc);
		String result = this.updateCurrency(c);
		System.out.println(result);
		
	}
	
	public String updateCurrency(Currency c) {
		String message = "";
		entityManagerUtilities.updateDB(Currency.class, c, c.getCurrencyCode());
		message += "Update Suceesfully " + c.getCurrencyCode() + "; ";
		return message;		
	}
}
