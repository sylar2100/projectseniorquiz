package com.main.currency;

import com.entities.Currency;
import com.transaction.EntityManagerUtilities;

public class DeleteCurrency {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Currency c) {
		String result = this.deleteCurrency(c);
		System.out.println(result);
		
	}
	
	public String deleteCurrency(Currency c) {
		String message = "";
		entityManagerUtilities.deleteDB(Currency.class, c.getCurrencyCode());
		message += "Remove Suceesfully " + c.getCurrencyCode() + "; ";
		return message;		
	}
}
