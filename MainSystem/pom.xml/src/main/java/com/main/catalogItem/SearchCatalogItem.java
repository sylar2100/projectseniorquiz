package com.main.catalogItem;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchCatalogItem {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public static SearchCatalogItem catalogItem = null;

	public List<Object> executeClass(String desc) {
		return this.searchAction(desc);		
	}
	
	public static SearchCatalogItem getInstance(){
		if(catalogItem == null){
			catalogItem = new SearchCatalogItem();
		}
		return catalogItem;
	}
	
	public List<Object> searchAction(String description) {
		String query = "from CatalogoItem where description like '%" + description + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveByCatalogId(String catId) {
		String query = "from CatalogoItem where id.catalogId = '" + catId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveDistinctByCatalogId(String catId) {
		String query = "select Distinct(id.productId) from CatalogoItem where id.catalogId = '" + catId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveDistinctByCatalogIdAndStatus(String catId, String status) {
		String query = "select Distinct(id.productId) from CatalogoItem where id.catalogId = '" + catId + "' and status='" + status + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveByCatalogIdAndProductId(String catId, String prodId) {
		String query = "from CatalogoItem where id.catalogId = '" + catId + "' and id.productId = '" + prodId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object retrieveByCatalogIdAndProductIdAndItemNumber(String catId, String prodId, String itemNumber) {
		Object result = null;
		String query = "from CatalogoItem where id.catalogId = '" + catId + "' and id.productId = '" + prodId + "' and id.itemNumber = '" + itemNumber + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);	
		if(queryResult.size() > 0) {
			result = queryResult.get(0);
		}
		return result;
	}
	
	public List<Object> searchBuyedItems(String catId, String prodId) {
		String query = "from CatalogoItem where status='05' and id.catalogId = '" + catId + "' and id.productId = '" + prodId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchStockItems(String catId, String prodId) {
		String query = "from CatalogoItem where (status='07' OR status='13') and id.catalogId = '" + catId + "' and id.productId = '" + prodId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchStockUserItems(String catId, String prodId, String itemNumber) {
		String query = "from CatalogoItem where (status='07' OR status='13') and id.catalogId = '" + catId + "' and id.productId = '" + prodId + "' and id.itemNumber = '" + itemNumber + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> searchDeliveredItems() {
		String query = "from CatalogoItem where status='08'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
