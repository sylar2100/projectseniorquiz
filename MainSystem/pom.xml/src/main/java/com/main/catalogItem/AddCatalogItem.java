package com.main.catalogItem;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogItemRepositoryImpl;
import com.common.HiltonUtility;
import com.common.UniqueKeyGenerator;
import com.entities.CatalogoItem;
import com.entities.CatalogoItemPk;
import com.entities.Currency;
import com.main.currency.SearchCurrency;

@Service
public class AddCatalogItem {
	@Autowired
	private CatalogItemRepositoryImpl catalogItemRepositoryImpl;
	public void executeClass(CatalogoItem cItem) {
		if(cItem != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			String template = HiltonUtility.ckNull(cItem.getTemplate());
			if((HiltonUtility.isEmpty(cItem.getAccountId()) || cItem.getAccountId().compareTo(BigDecimal.ZERO) == 0) && !template.equalsIgnoreCase("Y")) { 
				cItem.setAccountId(new BigDecimal(ukg.getUniqueKey()));
			}
			if(template.equalsIgnoreCase("Y")) { 
				cItem.getId().setItemNumber("S");
			} else {
				List<Object> queryResult = catalogItemRepositoryImpl.retrieveByCatalogIdAndProductId(cItem.getId().getCatalogId(),cItem.getId().getProductId());
				if(queryResult.size() > 0) {
					BigDecimal in = new BigDecimal(queryResult.size()).add(BigDecimal.ONE);
					cItem.getId().setItemNumber(in.toString());
				} else {
					cItem.getId().setItemNumber("1");
				}
			}
			cItem.setLastChangedBy("ADMIN");
			cItem.setLastChangedDate(new Date());
			cItem.setDateEntered(new Date());
			Currency result = (Currency) SearchCurrency.getInstance().searchById("USD");
			if(template.equalsIgnoreCase("Y")) { 	
				if(result != null) {
					cItem.setCost(cItem.getCost().multiply(result.getCurrencyChange()));
				}
			}
			catalogItemRepositoryImpl.insertCatalogoItem(cItem);
		}
	}
}
