package com.main.catalogItem;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogItemRepositoryImpl;
import com.entities.CatalogoItem;

@Service
public class UpdateCatalogItem {
	@Autowired
	private CatalogItemRepositoryImpl catalogItemRepositoryImpl;
	public void executeClass(CatalogoItem catalogItem) {
		if(catalogItem != null) {
			catalogItem.setLastChangedBy("ADMIN");
			catalogItem.setLastChangedDate(new Date());
			catalogItemRepositoryImpl.updateCatalogoItem(catalogItem);
		} 	
	}
}