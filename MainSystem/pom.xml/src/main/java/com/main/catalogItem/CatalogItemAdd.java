package com.main.catalogItem;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import com.common.DocumentStatus;
import com.common.HiltonUtility;
import com.entities.Catalogo;
import com.entities.CatalogoItem;
import com.entities.CatalogoItemPk;
import com.entities.Currency;
import com.entities.Vendor;
import com.main.catalog.SearchCatalog;
import com.main.currency.SearchCurrency;
import com.main.vendor.SearchVendor;
import com.transaction.EntityManagerUtilities;

public class CatalogItemAdd {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	CatalogoItem catalogItem = null;
	
	public void executeClass(CatalogoItem cItem) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		if(cItem == null) {
			cItem = new CatalogoItem();
		} 
		
		CatalogoItemPk cItemPk = new CatalogoItemPk();
		Scanner sc = new Scanner(System.in);
		Scanner scn = new Scanner(System.in);
		SearchCatalog searchCatalog = new SearchCatalog();
		SearchCatalogItem searchCatalogItem = new SearchCatalogItem();
		List<Object> queryResult = searchCatalog.allCatalogs();
		if(HiltonUtility.isEmpty(cItem.getId().getCatalogId())) {
			if(queryResult.size() > 0) {
				for(int c = 0; c < queryResult.size(); c++) {
					Catalogo catalog = (Catalogo) queryResult.get(c);
					System.out.println(catalog.getCatalogId());
				}
			}
			System.out.println("Catalog Id:");
			cItemPk.setCatalogId(sc.nextLine().toUpperCase());
		} else {
			cItemPk.setCatalogId(cItem.getId().getCatalogId());
		}
		if(HiltonUtility.isEmpty(cItem.getId().getProductId())) {
			queryResult = searchCatalogItem.retrieveDistinctByCatalogId(cItemPk.getCatalogId());
			if(queryResult.size() > 0) {
				for(int ci = 0; ci < queryResult.size(); ci++) {
					String productId = (String) queryResult.get(ci);
					System.out.println(productId);
				}
			}
			System.out.println("Product Id:");
			cItemPk.setProductId(sc.nextLine().toUpperCase());
		} else {
			cItemPk.setProductId(cItem.getId().getProductId());
		}
		queryResult = searchCatalogItem.retrieveByCatalogIdAndProductId(cItemPk.getCatalogId(),cItemPk.getProductId());
		if(queryResult.size() > 0) {
			BigDecimal in = new BigDecimal(queryResult.size()).add(BigDecimal.ONE);
			cItemPk.setItemNumber(in.toString());
		} else {
			cItemPk.setItemNumber("1");
		}
		cItem.setId(cItemPk);
		if(HiltonUtility.isEmpty(cItem.getDescription())) {
			System.out.println("Description:");
			cItem.setDescription(sc.nextLine());	
		}
		if(HiltonUtility.isEmpty(cItem.getVendorId())) {
			SearchVendor searchVendor = new SearchVendor();
			queryResult = searchVendor.allVendors();
			if(queryResult.size() > 0) {
				for(int c = 0; c < queryResult.size(); c++) {
					Vendor vendor = (Vendor) queryResult.get(c);
					System.out.println(vendor.getVendorId());
				}
			}
			System.out.println("Vendor Id:");
			cItem.setVendorId(sc.nextLine());
		}
		if(HiltonUtility.isEmpty(cItem.getImageFile())) {
			System.out.print("Introduzca ruta imagen: ");
			cItem.setImageFile(sc.nextLine() + ".jpg");
		} else {
			cItem.setImageFile(cItem.getImageFile() + ".jpg");
		}
		cItem.setLastChangedBy("ADMIN");
		cItem.setLastChangedDate(new Date());
		cItem.setDateEntered(new Date());
		
		if(HiltonUtility.isEmpty(cItem.getStatus())) {
			EnumSet<DocumentStatus> set = EnumSet.range(DocumentStatus.MadeOrder, DocumentStatus.Completed);
			set.add(DocumentStatus.ReadyForDeliver);
			set.add(DocumentStatus.Cancelled);
			for(DocumentStatus ds : set){
	            System.out.println( ds.name() + ":" + ds.getKey());
	        }
			System.out.println("Status:");
			cItem.setStatus(sc.nextLine());
		}
		
		if(HiltonUtility.isEmpty(cItem.getAccountId())) {
			System.out.println("Account Id:");
			cItem.setAccountId(scn.nextBigDecimal());
		}
		List<Object> resultList = SearchCurrency.getInstance().searchAll();
		for(int cur = 0; cur < resultList.size(); cur++) {
			Currency currency = (Currency) resultList.get(cur);
			System.out.println(currency.getCurrencyCode() + ":" + currency.getCurrencyName());
		}
		System.out.println("Currency Code Price:");
		String ccp = sc.nextLine();

		System.out.println("Price:");
		BigDecimal num = scn.nextBigDecimal();
		if(!ccp.equalsIgnoreCase("SOL")) {
			Currency result = (Currency) SearchCurrency.getInstance().searchById(ccp);
			if(result != null) {
				num = num.multiply(result.getCurrencyChange());
			}
		}
		cItem.setCost(num);
		
		if(HiltonUtility.isEmpty(cItem.getSalesCost())) {
			System.out.println("Sales Price:");
			cItem.setSalesCost(scn.nextBigDecimal());
		}
		
		if(HiltonUtility.isEmpty(cItem.getMarca())) {
			System.out.println("Marca:");
			cItem.setMarca(sc.nextLine());
		}
		
		if(HiltonUtility.isEmpty(cItem.getVersion())) {
			System.out.println("Version:");
			cItem.setVersion(sc.nextLine());
		}
		
		String result = this.insertCatalogItem(cItem);
		System.out.println(result);
		
	}
	
	public String insertCatalogItem(CatalogoItem cItem) {
		String message = "";
		entityManagerUtilities.addDB(cItem);
		this.setCatalogItem(cItem);
		message = "Add Successfully with Id " + cItem.getId().getProductId() + ":" + cItem.getId().getItemNumber() + ";";		
		return message;		
	}
	
	public CatalogoItem getCatalogItem() {
		return catalogItem;
	}

	public void setCatalogItem(CatalogoItem catalogItem) {
		this.catalogItem = catalogItem;
	}

}
