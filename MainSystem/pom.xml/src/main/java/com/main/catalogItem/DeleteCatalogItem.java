package com.main.catalogItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogItemRepositoryImpl;
import com.entities.CatalogoItem;

@Service
public class DeleteCatalogItem {
	@Autowired
	private CatalogItemRepositoryImpl catalogItemRepositoryImpl;
	public void executeClass(CatalogoItem catalogoItem) {
		if(catalogoItem != null) {
			catalogItemRepositoryImpl.deleteCatalogoItem(catalogoItem);
		} 	
	}
}
