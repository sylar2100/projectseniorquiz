package com.main.catalogItem;

import com.entities.CatalogoItem;
import com.transaction.EntityManagerUtilities;

public class CatalogItemDelete {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(CatalogoItem catalogItem) {
		String result = this.deleteCatalogoItem(catalogItem);
		System.out.println(result);
		
	}
	
	public String deleteCatalogoItem(CatalogoItem catalogItem) {
		String message = "";
		entityManagerUtilities.deleteDB(CatalogoItem.class, catalogItem.getId());
		message += "Remove Suceesfully " + catalogItem.getId().getProductId() + ":" + catalogItem.getId().getItemNumber() + ";";
		return message;		
	}
}
