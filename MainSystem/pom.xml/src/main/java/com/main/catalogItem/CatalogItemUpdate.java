package com.main.catalogItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Service;

import com.main.budget.SearchBudget;
import com.main.budget.UpdateBudget;
import com.main.currency.SearchCurrency;
import com.main.vendor.SearchVendor;
import com.transaction.EntityManagerUtilities;
import com.common.DocumentStatus;
import com.entities.Budget;
import com.entities.Catalogo;
import com.entities.CatalogoItem;
import com.entities.Currency;
import com.entities.Vendor;

@Service
public class CatalogItemUpdate {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	final String  pathImage = "C:\\JavaProjects\\DriveBudget\\workspace\\Images\\catalogs\\catalogItems";
	public void executeClass(CatalogoItem c) {
		Scanner sc = new Scanner(System.in);
		Scanner scn = new Scanner(System.in);
		String cad = "";
		System.out.println("Description:" + c.getDescription());
		cad = sc.nextLine();
		c.setDescription(cad.equals("-") ? c.getDescription() : cad);
		System.out.print("Introduzca imagen: ");
		cad = sc.nextLine();
		c.setImageFile(cad.equals("-") ? c.getImageFile() : pathImage + cad + ".jpg");
		
		SearchVendor searchVendor = new SearchVendor();
		List<Object> queryResult = searchVendor.allVendors();
		if(queryResult.size() > 0) {
			for(int v = 0; v < queryResult.size(); v++) {
				Vendor vendor = (Vendor) queryResult.get(v);
				System.out.println(vendor.getVendorId());
			}
		}
		System.out.println("Vendor Id:" + c.getVendorId());
		cad = sc.nextLine();
		c.setVendorId(cad.equals("-") ? c.getVendorId() : cad);
		
		EnumSet<DocumentStatus> set = EnumSet.range(DocumentStatus.MadeOrder, DocumentStatus.Completed);
		set.add(DocumentStatus.ReadyForDeliver);
		set.add(DocumentStatus.Cancelled);
		for(DocumentStatus ds : set){
            System.out.println( ds.name() + ":" + ds.getKey());
        }
		System.out.println("Status:" + c.getStatus());
		cad = sc.nextLine();
		c.setStatus(cad.equals("-") ? c.getStatus() : cad);
		
		c.setLastChangedBy("ADMIN");
		c.setLastChangedDate(new Date());
		
		System.out.println("Account Id:" + c.getAccountId().toString());
		BigDecimal num = scn.nextBigDecimal();
		c.setAccountId(num.compareTo(BigDecimal.ZERO) == 0 ? c.getAccountId() : num);
		
		System.out.println("Currency Code Price:");
		String ccp = sc.nextLine();
		
		System.out.println("Price:" + c.getCost());
		num = scn.nextBigDecimal();
		if(!ccp.equalsIgnoreCase("SOL")) {
			Currency result = (Currency) SearchCurrency.getInstance().searchById(ccp);
			if(result != null) {
				num = num.multiply(result.getCurrencyChange());
			}
		}
		c.setCost(num.compareTo(BigDecimal.ZERO) == 0 ? c.getCost() : num);
		
		System.out.println("Sales Price:" + c.getSalesCost());
		num = scn.nextBigDecimal();
		c.setSalesCost(num.compareTo(BigDecimal.ZERO) == 0 ? c.getSalesCost() : num);
		String status = DocumentStatus.Completed.getKey();
		if(status.equals(c.getStatus())) {
			HashMap<String, Budget> budgetMap = new HashMap<String, Budget>();
			BigDecimal gain = c.getSalesCost().subtract(c.getCost());
			Currency result = (Currency) SearchCurrency.getInstance().searchById("USD");
			BigDecimal buyDollars = BigDecimal.ZERO;
			if(result != null) {
				buyDollars = c.getCost().divide(result.getCurrencyChange(),RoundingMode.HALF_UP);
			}
			SearchBudget searchBudget = new SearchBudget();
			queryResult = searchBudget.retrieveAll();
			if(queryResult.size() > 0) {
				for(int up = 0; up < queryResult.size(); up++) {
					Budget budget = (Budget) queryResult.get(up);					
					System.out.println(budget.getBudgetId() + ":" + budget.getDescription());
					budgetMap.put(budget.getBudgetId().toString(), budget);
				}
			}
			System.out.println("Update Budget Gain:");
			UpdateBudget.getInstance().updateBudgetAmount(budgetMap.get(sc.nextLine()), gain, "Y");
			System.out.println("Update Budget Buyed Dollars:");
			UpdateBudget.getInstance().updateBudgetAmount(budgetMap.get(sc.nextLine()), buyDollars, "Y");
		}
		String result = this.updateCatalogItem(c);
		System.out.println(result);
		
	}
	
	public String updateCatalogItem(CatalogoItem catalogItem) {
		String message = "";
		entityManagerUtilities.updateDB(CatalogoItem.class, catalogItem, catalogItem.getId());
		message += "Update Suceesfully " + catalogItem.getId().getProductId() + ":" + catalogItem.getId().getItemNumber() + ";";
		return message;		
	}
}
