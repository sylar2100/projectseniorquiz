package com.main.company;

import com.entities.Company;
import com.transaction.EntityManagerUtilities;

public class DeleteCompany {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Company company) {
		String result = this.deleteCompany(company);
		System.out.println(result);
		
	}
	
	public String deleteCompany(Company company) {
		String message = "";
		entityManagerUtilities.deleteDB(Company.class, company.getCompanyId());
		message += "Remove Suceesfully " + company.getCompanyId().toString() + "; ";
		return message;		
	}
}
