package com.main.company;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import com.main.currency.SearchCurrency;
import com.transaction.EntityManagerUtilities;
import com.entities.Capital;
import com.entities.Company;
import com.entities.Currency;

public class UpdateCompany {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Company c) {
		Scanner sc = new Scanner(System.in);
		String cad = "";
		System.out.println("Description:" + c.getDescription());
		cad = sc.nextLine();
		c.setDescription(cad.equals("-") ? c.getDescription() : cad);
		System.out.println("Address:" + c.getAddress());
		cad = sc.nextLine();
		c.setAddress(cad.equals("-") ? c.getAddress() : cad);
		System.out.println("Email Address:" + c.getEmailAddress());
		cad = sc.nextLine();
		c.setEmailAddress(cad.equals("-") ? c.getEmailAddress() : cad);
		System.out.println("Celular:" + c.getCelNumber());
		cad = sc.nextLine();
		c.setCelNumber(cad.equals("-") ? c.getCelNumber() : cad);
		System.out.println("Currency Code:" + c.getCurrencyCode());
		LinkedHashMap<String,String> currMap = new LinkedHashMap<String,String>();
		List<Object> resultList = SearchCurrency.getInstance().searchAll();
		for(int cur = 0; cur < resultList.size(); cur++) {
			Currency currency = (Currency) resultList.get(cur);
			currMap.put(currency.getCurrencyCode(), currency.getCurrencyName());
			System.out.println(currency.getCurrencyCode() + ":" + currency.getCurrencyName());
		}
		cad = sc.nextLine();
		c.setCurrencyCode(cad.equals("-") ? c.getCurrencyCode() : cad);
		
		String result = this.updateCompany(c);
		System.out.println(result);
		
	}
	
	public String updateCompany(Company c) {
		String message = "";
		entityManagerUtilities.updateDB(Company.class, c, c.getCompanyId());
		message += "Update Suceesfully " + c.getCompanyId().toString() + "; ";
		return message;		
	}
}
