package com.main.company;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchCompany {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> executeClass(String name) {
		return this.searchAction(name);		
	}
	
	public List<Object> searchAction(String name) {
		String query = "from Company where name like '%" + name + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
