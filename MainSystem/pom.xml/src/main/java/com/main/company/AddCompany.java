package com.main.company;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.common.UniqueKeyGenerator;
import com.entities.Capital;
import com.entities.Company;
import com.entities.Currency;
import com.main.currency.SearchCurrency;
import com.transaction.EntityManagerUtilities;

public class AddCompany {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public void executeClass(Company company) {
		Company c = new Company();
		Scanner sc = new Scanner(System.in);
		if(company != null) {
			c.setName(company.getName());
		} else {
			System.out.println("Name:");
			c.setName(sc.nextLine());
		}
		UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
		c.setCompanyId(new BigDecimal(ukg.getUniqueKey()));
		System.out.println("Description:");
		c.setDescription(sc.nextLine());
		System.out.println("Address:");
		c.setAddress(sc.nextLine());
		System.out.println("Email Address:");
		c.setEmailAddress(sc.nextLine());
		System.out.println("Celular:");
		c.setCelNumber(sc.nextLine());
		System.out.println("Currency Code:");
		LinkedHashMap<String,String> currMap = new LinkedHashMap<String,String>();
		List<Object> resultList = SearchCurrency.getInstance().searchAll();
		for(int cur = 0; cur < resultList.size(); cur++) {
			Currency currency = (Currency) resultList.get(cur);
			currMap.put(currency.getCurrencyCode(), currency.getCurrencyName());
			System.out.println(currency.getCurrencyCode() + ":" + currency.getCurrencyName());
		}
		c.setCurrencyCode(sc.nextLine());

		String result = this.insertCompany(c);
		System.out.println(result);
		
	}
	
	public String insertCompany(Company c) {
		String message = "";
		entityManagerUtilities.addDB(c);
		message = "Add Successfully with Id " + c.getCompanyId().toString();		
		return message;		
	}
}
