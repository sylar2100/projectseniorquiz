package com.main.capital;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CapitalRepositoryImpl;
import com.entities.Capital;

@Service
public class DeleteCapital {
	@Autowired
	private CapitalRepositoryImpl capitalRepositoryImpl;
	public void executeClass(Capital capital) {
		if(capital != null) {
			capitalRepositoryImpl.deleteCapital(capital);
		} 	
	}
}