package com.main.capital;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CapitalRepositoryImpl;
import com.common.UniqueKeyGenerator;
import com.entities.Capital;

@Service
public class AddCapital {
	@Autowired
	private CapitalRepositoryImpl capitalRepositoryImpl;
	public void executeClass(Capital capital) {
		if(capital != null) {
			UniqueKeyGenerator ukg = UniqueKeyGenerator.getInstance();
			capital.setCapitalId(new BigDecimal(ukg.getUniqueKey()).toString());
			capital.setCapitalDate(new Date());
			capitalRepositoryImpl.insertCapital(capital);
		} 
	}
}