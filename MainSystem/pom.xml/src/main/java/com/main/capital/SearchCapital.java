package com.main.capital;

import java.math.BigDecimal;
import java.util.List;

import com.main.budget.SearchBudget;
import com.transaction.EntityManagerUtilities;

public class SearchCapital {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public static SearchCapital searchCapital = null;
	
	public static SearchCapital getInstance(){
		if(searchCapital == null){
			searchCapital = new SearchCapital();
		}
		return searchCapital;
	}
	public List<Object> executeClass(String description, String capitalPlace) {
		return this.searchAction(description, capitalPlace);		
	}
	
	public List<Object> searchAction(String description, String capitalPlace) {
		String query = "from Capital where description like '%" + description + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public Object retrieveById(String capitalId) {
		Object capital = null;
		String query = "from Capital where capitalId = '" + capitalId + "'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);
		if(queryResult.size() > 0) {
			capital = queryResult.get(0);
		}
		return capital;
	}
	
	public List<Object> retrieveByClassB() {
		String query = "from Capital where classCapital = 'B' and status = '01'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);

		return queryResult;
	}
	
	public List<Object> retrieveByClassA() {
		String query = "from Capital where classCapital = 'A' and capitalPlace <> 'CASA' and status = '01'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);

		return queryResult;
	}
	
	public List<Object> retrieveAll() {
		String query = "from Capital";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
