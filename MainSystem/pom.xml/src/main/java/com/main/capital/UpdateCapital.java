package com.main.capital;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.main.budget.UpdateBudget;
import com.main.currency.SearchCurrency;
import com.transaction.EntityManagerUtilities;
import com.budget.repository.CapitalRepositoryImpl;
import com.entities.Budget;
import com.entities.Capital;
import com.entities.Currency;

@Service
public class UpdateCapital {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public static UpdateCapital updateCapital = null;
	@Autowired
	private CapitalRepositoryImpl capitalRepositoryImpl;
	public void executeClass(Capital capital) {
		if(capital != null) {
			capitalRepositoryImpl.updateCapital(capital);
		} 	
	}
	
	public static UpdateCapital getInstance(){
		if(updateCapital == null){
			updateCapital = new UpdateCapital();
		}
		return updateCapital;
	}
		
	public void updateCapitalAmount(Capital c, BigDecimal amount, String add) {
		String message = "";
		if(add.equalsIgnoreCase("Y")) {
			c.setAmount(c.getAmount().add(amount));
		} else {
			c.setAmount(c.getAmount().subtract(amount));
		}
		entityManagerUtilities.updateDB(Capital.class, c, c.getCapitalId());
		message += "Update Suceesfully " + c.getCapitalId() + "; ";
		System.out.println(message);		
	}
}
