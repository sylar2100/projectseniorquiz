package com.main.catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogRepositoryImpl;
import com.entities.Catalogo;

@Service
public class DeleteCatalog {
	@Autowired
	private CatalogRepositoryImpl catalogRepositoryImpl;
	public void executeClass(Catalogo catalog) {
		if(catalog != null) {
			catalogRepositoryImpl.deleteCatalog(catalog);
		} 	
	}
}
