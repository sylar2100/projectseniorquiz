package com.main.catalog;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogRepositoryImpl;
import com.entities.Catalogo;

@Service
public class UpdateCatalog {
	@Autowired
	private CatalogRepositoryImpl catalogRepositoryImpl;
	public void executeClass(Catalogo catalog) {
		if(catalog != null) {
			catalog.setLastChangedBy("ADMIN");
			catalog.setLastChangedDate(new Date());
			catalogRepositoryImpl.updateCatalog(catalog);
		} 	
	}
}
