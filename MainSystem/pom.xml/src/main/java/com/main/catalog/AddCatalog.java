package com.main.catalog;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.CatalogRepositoryImpl;
import com.budget.services.InitServiceData;
import com.entities.Catalogo;

@Service
public class AddCatalog {
	@Autowired
	private CatalogRepositoryImpl catalogRepositoryImpl;
	@Autowired
	private InitServiceData initServiceData;
	public void executeClass(Catalogo catalog) {
		if(catalog != null) {
			catalog.setLastChangedBy("ADMIN");
			catalog.setLastChangedDate(new Date());
			catalog.setDateEntered(new Date());
			catalogRepositoryImpl.insertCatalog(catalog);
			initServiceData.cleanList();
		}
	}
}
