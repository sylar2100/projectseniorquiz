package com.main.catalog;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchCatalog {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> executeClass(String catalogId) {
		return this.searchAction(catalogId);		
	}
	
	public List<Object> searchAction(String catalogId) {
		String query = "from Catalogo where catalogId like '%" + catalogId + "%'";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> allCatalogs() {
		String query = "from Catalogo";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
