package com.main.userProfile;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.UserProfileRepositoryImpl;
import com.entities.UserProfile;

@Service
public class AddUserProfile {
	@Autowired
	private UserProfileRepositoryImpl userProfileRepositoryImpl;
	public void executeClass(UserProfile user) {
		if(user != null) {
			String id = user.getFirstName().substring(0,user.getFirstName().length() >= 5 ? 5 : user.getFirstName().length()) + user.getLastName().substring(0,user.getLastName().length() >= 5 ? 5 : user.getLastName().length());
			user.setUserId(id.toUpperCase().trim());
			user.setOwner("ADMIN");
			user.setLastChangedBy("ADMIN");;
			user.setLastChangedDate(new Date());
			user.setDateEntered(new Date());
			userProfileRepositoryImpl.insertUser(user);
		} 
	}
	
}