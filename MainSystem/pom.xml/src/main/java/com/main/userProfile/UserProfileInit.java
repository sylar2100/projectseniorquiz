package com.main.userProfile;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.UserProfileRepositoryImpl;
import com.entities.UserProfile;

@Service
public class UserProfileInit {
	@Autowired
	public UserProfileRepositoryImpl userProfileRepositoryImpl;
	public static List<UserProfile> userProfileList = new ArrayList<UserProfile>();
	
	public List<UserProfile> getInstance() {
		if(userProfileList.size() < 1) {
			this.loadAll();
		}
		return userProfileList;
	}
	
	public void loadAll() {
		List<Object> listUsers = userProfileRepositoryImpl.findAll();
		if(listUsers != null && listUsers.size() > 0) {
			listUsers.forEach(user -> userProfileList.add((UserProfile) user));
		}
	}
	
	public void cleanList() {
		userProfileList.clear();
	}
}
