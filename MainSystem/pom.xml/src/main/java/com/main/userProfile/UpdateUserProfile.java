package com.main.userProfile;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.UserProfileRepositoryImpl;
import com.entities.UserProfile;

@Service
public class UpdateUserProfile {
	@Autowired
	private UserProfileRepositoryImpl userProfileRepositoryImpl;
	public void executeClass(UserProfile user) {
		if(user != null) {
			user.setLastChangedBy("ADMIN");;
			user.setLastChangedDate(new Date());
			userProfileRepositoryImpl.updateUser(user);
		}
	}
}