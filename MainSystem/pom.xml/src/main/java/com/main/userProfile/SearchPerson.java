package com.main.userProfile;

import java.util.List;

import com.transaction.EntityManagerUtilities;

public class SearchPerson {
	EntityManagerUtilities entityManagerUtilities = new EntityManagerUtilities();
	public List<Object> executeClass(String firstName, String lastName) {
		String id = firstName.substring(0,firstName.length() >= 5 ? 5 : firstName.length()) + lastName.substring(0,lastName.length() >= 5 ? 5 : lastName.length());
		
		return this.searchAction(id.toUpperCase(), firstName, lastName);		
	}
	
	public List<Object> searchAction(String id, String firstName, String lastName) {
		String query = "from UserProfile where (userId = '" + id + "' or (firstName like '%" + firstName + "%' and lastName like '%" + lastName + "%'))";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveAllRequesters() {
		String query = "from UserProfile where requisitioner='Y' order by userId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
	
	public List<Object> retrieveAllBuyers() {
		String query = "from UserProfile where buyer='Y' order by userId";
		List<Object> queryResult = entityManagerUtilities.findSessionDB(query);		
		return queryResult;
	}
}
