package com.main.userProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.repository.UserProfileRepositoryImpl;
import com.entities.UserProfile;

@Service
public class DeleteUserProfile {
	@Autowired
	private UserProfileRepositoryImpl userProfileRepositoryImpl;
	public void executeClass(UserProfile user) {
		if(user != null) {
			userProfileRepositoryImpl.deleteUser(user);
		} 	
	}
}
