package com.entities;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ResponseObject {
	private String message;
	private String statusCode;
	private Object objResponse;
	private List<Object> objResponseList;
	private List<Users> userProfileList;
	private HashMap<String,HashMap<String,String>> mapData;
	public HashMap<String,HashMap<String, String>> getMapData() {
		return mapData;
	}
	public void setMapData(HashMap<String,HashMap<String,String>> mapData) {
		this.mapData = mapData;
	}
	public List<Users> getUserProfileList() {
		return userProfileList;
	}
	public void setUserProfileList(List<Users> userProfileList) {
		this.userProfileList = userProfileList;
	}
	public List<Object> getObjResponseList() {
		return objResponseList;
	}
	public void setObjResponseList(List<Object> objResponseList) {
		this.objResponseList = objResponseList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public Object getObjResponse() {
		return objResponse;
	}
	public void setObjResponse(Object objResponse) {
		this.objResponse = objResponse;
	}
	
}