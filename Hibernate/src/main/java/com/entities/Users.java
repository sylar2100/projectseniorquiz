package com.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.common.HiltonUtility;
import com.fasterxml.jackson.annotation.JsonFormat;

/** @author Hibernate CodeGenerator */
@Entity
@Table(name="USERS")
public class Users {
	@Id
    @Column(name="USER_ID")
    private String userId;
	
	@Column(name="MAIL_ID")
    private String mailId;
	
	@Column(name="FIRST_NAME")
    private String firstName;
	
	@Column(name="LAST_NAME")
    private String lastName;
	
	@Column(name="USER_PASSWORD")
    private String userPassword;
	
	@Column(name="PHONE_NUMBER")
    private String phoneNumber;
	
	@Column(name="ADDRESS")
    private String address;
	
	@Column(name="DISTRICT")
    private String district;
	
	@Column(name="CITY")
    private String city;
    
	@Column(name="COUNTRY")
    private String country;
	
	@Column(name="DNI")
    private String dni;
	
	@Column(name="COINS")
    private BigDecimal coins;

	/** full constructor */
    public Users( final String userId, final String mailId, final String firstName, final String lastName, final String phoneNumber, final String address, final String district, final String city, final String country, final String userPassword, final BigDecimal coins) {
    	this.userId = userId;
        this.mailId = mailId;
    	this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.district = district;
        this.city = city;
        this.country = country;
        this.userPassword = userPassword;
        this.coins = coins;
    }

    /** default constructor */
    public Users() {
    }

    /** minimal constructor */
    public Users(final java.lang.String userId) {
        this.userId = userId;
    }

    public java.lang.String getUserId() {
		return (java.lang.String)HiltonUtility.ckNull(this.userId).trim();
    }

	public void setUserId(java.lang.String userId) {
		if (!HiltonUtility.isEmpty(userId) && userId.length() > 256) {
			userId = userId.substring(0, 256);
		}
		this.userId = userId;
	}

    public java.lang.String getMailId() {
		return HiltonUtility.ckNull(this.mailId);
    }

    public void setMailId(java.lang.String mailId) {
        if (!HiltonUtility.isEmpty(mailId)) {
            mailId = mailId.toLowerCase();
        }
    	this.mailId = mailId;
    }

    public java.lang.String getFirstName() {
		return (java.lang.String)HiltonUtility.ckNull(this.firstName).trim();
    }

    public void setFirstName(java.lang.String firstName) {
		if (!HiltonUtility.isEmpty(firstName) && firstName.length() > 128) {
			firstName = firstName.substring(0, 128);
		}
		this.firstName = firstName;
    }

    public java.lang.String getLastName() {
		return (java.lang.String)HiltonUtility.ckNull(this.lastName).trim();
    }

    public void setLastName(java.lang.String lastName) {
		if (!HiltonUtility.isEmpty(lastName) && lastName.length() > 128) {
			lastName = lastName.substring(0, 128);
		}
		this.lastName = lastName;
    }

    public java.lang.String getPhoneNumber() {
		return (java.lang.String)HiltonUtility.ckNull(this.phoneNumber).trim();
    }

    public void setPhoneNumber(java.lang.String phoneNumber) {
		if (!HiltonUtility.isEmpty(phoneNumber) && phoneNumber.length() > 64) {
			phoneNumber = phoneNumber.substring(0, 64);
		}
		this.phoneNumber = phoneNumber;
    }
    
    public java.lang.String getAddress() {
		return (java.lang.String)HiltonUtility.ckNull(this.address).trim();
    }

    public void setAddress(java.lang.String address) {
		this.address = address;
    }
    
    public java.lang.String getDistrict() {
		return (java.lang.String)HiltonUtility.ckNull(this.district).trim();
    }

    public void setDistrict(java.lang.String district) {
		this.district = district;
    }
    
    public java.lang.String getCity() {
		return (java.lang.String)HiltonUtility.ckNull(this.city).trim();
    }

    public void setCity(java.lang.String city) {
		this.city = city;
    }
    
    public java.lang.String getCountry() {
		return (java.lang.String)HiltonUtility.ckNull(this.country).trim();
    }

    public void setCountry(java.lang.String country) {
		this.country = country;
    }
   
    public java.lang.String getUserPassword() {
		return HiltonUtility.ckNull(this.userPassword);
    }

    public void setUserPassword(java.lang.String userPassword) {
		if (!HiltonUtility.isEmpty(userPassword) && userPassword.length() > 30) {
			userPassword = userPassword.substring(0, 30);
		}
		this.userPassword = userPassword;
    }

    public BigDecimal getCoins() {
		return coins;
	}

	public void setCoins(BigDecimal coins) {
		this.coins = coins;
	}

    public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
}
