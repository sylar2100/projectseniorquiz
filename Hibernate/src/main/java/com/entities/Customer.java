package com.entities;

import org.springframework.stereotype.Component;

@Component
public class Customer {
	private String handler;
	private String operation;
	private String browseName;

	private Users user;
	
	private TransferObject transferObject;

	public Customer(){}
	
	public Customer(String handler, String operation){
		this.handler = handler;
		this.operation = operation;
	}

	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}
	
	public TransferObject getTransferObject() {
		return transferObject;
	}

	public void setTransferObject(TransferObject transferObject) {
		this.transferObject = transferObject;
	}
	
	public String getBrowseName() {
		return browseName;
	}

	public void setBrowseName(String browseName) {
		this.browseName = browseName;
	}
	
}