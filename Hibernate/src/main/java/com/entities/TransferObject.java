package com.entities;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class TransferObject {
	private String userSend;
	private BigDecimal transferCoin;
	private String userReceive;

	public TransferObject(){}
	
	public TransferObject(String userSend, String userReceive){
		this.userSend = userSend;
		this.userReceive = userReceive;
	}

	public String getUserSend() {
		return userSend;
	}

	public void setUserSend(String userSend) {
		this.userSend = userSend;
	}

	public BigDecimal getTransferCoin() {
		return transferCoin;
	}

	public void setTransferCoin(BigDecimal transferCoin) {
		this.transferCoin = transferCoin;
	}

	public String getUserReceive() {
		return userReceive;
	}

	public void setUserReceive(String userReceive) {
		this.userReceive = userReceive;
	}
	
}