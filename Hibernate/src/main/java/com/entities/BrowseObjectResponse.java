package com.entities;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class BrowseObjectResponse {
	private String message;
	private String statusCode;
	private String imagePath;
	private boolean needButtons;
	private boolean viewButton;
	private List<Object> objResponse;
	private List<String> columns;
	private List<String> labels;
	public boolean getNeedButtons() {
		return needButtons;
	}
	public void setNeedButtons(boolean needButtons) {
		this.needButtons = needButtons;
	}
	public boolean getViewButton() {
		return viewButton;
	}
	public void setViewButton(boolean viewButton) {
		this.viewButton = viewButton;
	}
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<String> getColumns() {
		return columns;
	}
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<Object> getObjResponse() {
		return objResponse;
	}
	public void setObjResponse(List<Object> objResponse) {
		this.objResponse = objResponse;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
}