package com.transaction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class EntityManagerUtilities {
	private static String bdName = "Account";
	public void addDB(Object obj) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(bdName);
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(obj);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			em.close();
		}
	}
	
	public void updateDB(Class<?> objName, Object objectUpdate, Object key) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(bdName);
		EntityManager em = emf.createEntityManager();
		try {
			
			em.getTransaction().begin();
	
	        Object obj = em.find(objName, key);
	         
	        if(obj != null) {
		        //Detach entity
		        em.clear();

		        em.merge(objectUpdate);
	
		        em.getTransaction().commit();
		        System.out.println("Updated successfully");
	        }
	         
		} catch (Exception e) {
	         e.printStackTrace();
	         if (em != null) {
	            System.out.println("Transaction is being rolled back.");
	            em.getTransaction().rollback();
	         }
	     } finally {
	         if (em != null) {
	        	 em.close();
	         }
	     }
	}
	
	public void deleteDB(Class<?> objName, Object key) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(bdName);
		EntityManager em = emf.createEntityManager();
		try {
			Object objFind = em.find(objName, key);
			em.getTransaction().begin();
			em.remove(objFind);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.close();
		}
	}
	
	public Object findDB(Object obj, Object key) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(bdName);
		EntityManager em = emf.createEntityManager();
		obj = em.find(obj.getClass(), key);
		return obj;
	}
	
	public List<Object> findSessionDB(String queryString) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(bdName);
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(queryString);
		List<Object> objList = new ArrayList<Object>();
		
        objList  = query.getResultList();

        return objList;
	}
}
