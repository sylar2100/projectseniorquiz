package com.common;

public enum DocumentStatus {

	Block("00","Block"), 
	
    Active("01","Active"),

    Inactive("02","Inactive"),

    MadeOrder("03","MadeOrder"),
    
    PartiallyPaid("04","PartiallyPaid"), 

    Buyed("05","Buyed"),
    
    Shipped("06","Shipped"),
    
    Stock("07","Stock"),
    
    Delivered("08","Delivered"),
    
    Completed("09","Completed"),
    
    Separated("10","Separated"),
    
    WaitPayment("11","WaitPayment"),
    
    Paid("12","Paid"), 
    
    ReadyForDeliver("13","ReadyForDeliver"),
    
    Cancelled("14","Cancelled"),
	
	Closed("15","Closed");

 
	private final String key;
    private final String value;

    DocumentStatus(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }
    public String getValue() {
        return value;
    }
} 