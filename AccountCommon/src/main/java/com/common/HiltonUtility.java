/*

 * Created on March 1, 2003
 */
package com.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Johann
 */
public class HiltonUtility
{
	/**
	*  isEmpty Method
	*  Tests for an empty array
	*  @param  test Object[]
	*  @return boolean
	*/
	public static boolean isEmpty(Object[] test) {
		boolean lbEmpty = false;

		if (test == null || test.length <= 0) {
			lbEmpty = true;
		}

		return lbEmpty;
	}
	
	/**
	*  isEmpty Method
	*  Tests for an empty string
	*  @param  test String
	*  @return boolean
	*/
	public static boolean isEmpty(String test) {
		boolean lbEmpty = false;

		if (test == null || test.trim().length() <= 0) {
			lbEmpty = true;
		}

		return lbEmpty;
	}
	
	/**
	*  isEmpty Method
	*  Tests for an empty string
	*  @param  test Object
	*  @return boolean
	*/
	public static boolean isEmpty(Object test) {
		boolean lbEmpty = false;

		if (test == null) {
			lbEmpty = true;
		}else if(test instanceof String){
			lbEmpty = HiltonUtility.isEmpty(((String)test));
		}

		return lbEmpty;
	}
	
	/**
	*  isEmpty Method
	*  Tests for an empty bigDecimal
	*  @param  test BigDecimal
	*  @return boolean
	*/
	public static boolean isEmpty(BigDecimal test) {
		boolean lbEmpty = false;

		if (test == null || test.compareTo(new BigDecimal(0)) == 0 ) {
			lbEmpty = true;
		}

		return lbEmpty;
	}

	/**
	 *  isEmpty Method
	 *  Tests for an empty list
	 *  @param  test java.util.List
	 *  @return boolean
	 */
	public static boolean isEmpty(List test) {
		boolean lbEmpty = false;

		if (test == null || test.size() <= 0) {
			lbEmpty = true;
		}

		return lbEmpty;
	}
	
	/**
	 *  isEmpty Method
	 *  Tests for an empty map
	 *  @param  test java.util.Map
	 *  @return boolean
	 */
	public static boolean isEmpty(Map test) {
		boolean lbEmpty = false;

		if (test == null || test.size() <= 0 || test.isEmpty()) {
			lbEmpty = true;
		}

		return lbEmpty;
	}
	
	/**
		*  ckNull Method
		*  @param  test String value to test - set to an empty string if null
		*  @return String
		*/
	public static String ckNull(String test) {
		if (test == null) {
			test = "";
		} else if (test instanceof String) {
			String testString = (String) test;
			if (testString.length() <= 0) {
				test = "";
			}
		}

		return test;
	}

	/**
		*  ckNullBigDecimal Method
		*  @param  test BigDecimal value to test - set zero if null
		*  @return BigDecimal
		*/
	public static BigDecimal ckNull(BigDecimal test) {
		if (test == null) {
			test = new BigDecimal(0);
		}
		return test;
	}

	public static Integer ckNull(Integer test) {
		if (test == null) {
			test = new Integer(0);
		}
		return test;
	}

	public static java.util.Date ckNull(java.util.Date test) {
		if (test == null) {
			test = new java.util.Date();
		}
		return test;
	}

	public static java.sql.Date ckNull(java.sql.Date test) {
		if (test == null) {
			test = new java.sql.Date((new java.util.Date()).getTime());
		}
		return test;
	}

	public static java.util.List ckNull(java.util.List test) {
		if (test == null) {
			test = new ArrayList();
		}
		return test;
	}
	
	public static java.util.Map ckNull(java.util.Map test) {
		if (test == null) {
			test = new HashMap();
		}
		return test;
	}
	

	public static Object ckNull(Object o)
	{
		if(o == null)
		{
			if(o instanceof String)
			{
				o = "";
			}
			else if(o instanceof BigDecimal)
			{
				o = new BigDecimal(0);
			}
			else if(o instanceof Integer)
			{
				o = new Integer(0);
			}
			else if(o instanceof java.util.Date)
			{
				o = new java.util.Date();
			}
			else if(o instanceof java.sql.Date)
			{
				o = new java.sql.Date((new java.util.Date()).getTime());
			}
		}
		return o;
	}
	
	public static Date sumarDiasAFecha(Date fecha, int dias) {
	      if (dias==0) return fecha;
	      Calendar calendar = Calendar.getInstance();
	      calendar.setTime(fecha); 
	      calendar.add(Calendar.DAY_OF_YEAR, dias);  
	      return calendar.getTime(); 
	}
}
